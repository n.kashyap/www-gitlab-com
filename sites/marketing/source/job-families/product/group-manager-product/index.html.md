---
layout: job_family_page
title: "Group Manager, Product"
---

As the Group Manager, Product, you will be responsible for managing and building a team that focuses on a [specific](#specialties) GitLab collection of groups from our [product hierarchy](/handbook/product/product-categories/#hierarchy) ([sometimes encompassing entire stages or a section](/handbook/product/product-leadership/#product-organizational-structure)).  This role typically manages 2-4 Product Managers, and reports either to a Director of Product or the VP of Product.

### Job Grade 

The Group Manager, Product is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Individual responsibility

- Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Work on the vision with the Director of Product, VP of Product Strategy, VP of Product, and CEO; and communicate this vision internally and externally
- Distill the overall vision into a compelling roadmap
- Make sure the vision advances in every release and communicate this
- Communicate our vision though demo's, conference speaking, blogging, and interviews
- Work closely with Product Marketing, Sales, Engineering, etc.

### Team responsibility

- Ensure that the next milestone contains the most relevant items to customers, users, and us
- Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
- Make sure the [DevOps tools](/devops-tools/) are up to date
- Keep relevant [/direction](/direction) pages up to date as our high level roadmap
- Regularly join customer and partner visits that can lead to new features
- Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
- Make sure the release announcements are attractive and cover everything
- Be present on social media (hacker news, twitter, stack overflow, mailinglist), especially around releases

### Requirements

* 1-3 years experience managing others
* 4-6 years of experience in product management
* Experience hiring teams in high growth companies
* Experience in DevOps
* Experience with solutions from the product category, groups or DevOps stages you will be responsible for
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* Additional requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework)
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values), and work in accordance with those values
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
* Ability to use GitLab

## Primary Performance Indicators for the Role
* [Stage Monthly Active Users](https://about.gitlab.com/handbook/product/metrics/#adoption)
* [Category Maturity Achievement](https://about.gitlab.com/handbook/product/metrics/)

## Relevant Links
* [Product Handbook](https://about.gitlab.com/handbook/product/)
* [Product Leadership](https://about.gitlab.com/handbook/product/product-leadership/)
* [Product Development Workflow](https://about.gitlab.com/handbook/product-development-flow/)
* [Product Management Career Development Framework](https://about.gitlab.comhttps://about.gitlab.com/handbook/product/product-manager-role/#product-management-career-development-framework)
* [Engineering Workflow](https://about.gitlab.com/handbook/engineering/workflow)

## Specialties
Group Managers, Product are assigned a collection of groups from our [product hierarchy](/handbook/product/product-categories/#hierarchy) ([sometimes encompassing entire stages or section](/handbook/product/product-leadership/#product-organizational-structure)) and manage groups of Product Managers within that stage.

### Fulfillment

You will be responsible for the [Fulfillment](https://about.gitlab.com/direction/fulfillment/) product team, which owns delivering a flexible and powerful billing and licensing system.  You'll have an opportunity to work cross-functionally to ensure the billing and licensing system meets our evolving business needs and delivers a world class customer experience.

#### Requirements

- Strong business operations skills and an ability to think systematically about complex workflows and work cross-functionally to ensure an excellent end-to-end customer experience
- Previous experience with high scale billing & licensing systems
- Familiarity with commercial best practices for no touch, sales assisted, and partner assisted transactions
- Excellent communication skills at all levels, including e-group

### Verify

You will be responsible for managing the team of product managers covering the [Verify stage groups](/handbook/product/product-categories/#verify-stage). Leading the [Verify direction](/direction/ops/#verify) is a high impact position. You'll work with your team to ensure our [industry-leading CI](/resources/forrester-wave-cloudnative-ci/) continues to be a [critical entry-point](/handbook/marketing/product-marketing/usecase-gtm/ci/) to our single devops platform. You'll have the opportunity to work cross functionally as an expert and invest in your team of high caliber product managers to make sure our [R&D investment in Verify](/handbook/product/investment/#investment-by-stage) is as effective as possible in fulfilling our [mission to enable everyone to contribute](/company/strategy/#mission).

## Career Ladder

The next step in the Group Manager, Product job family is to move to the [Director of Product](/job-families/product/director-of-product) job family. 

## Hiring Process

Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Product
* Next, candidates will be invited to schedule a 45 minute peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute deep dive interview with a member of the Product team
* Next, candidates will be invited to schedule a 45 minute direct report panel interview with members of the Product team
* Next, candidates will be invited to schedule a 45 minute fifth interview with a VP of Product or above
* Successful candidates will subsequently be made an offer via email

