---
layout: markdown_page
title: "Dogfood Plan Working Group"
description: "This working group will integrate new Plan stage features in GitLab’s own agile planning process."
canonical_path: "/company/team/structure/working-groups/dogfood-plan/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property     | Value            |
|--------------|------------------|
| Date Created | August 1, 2020   |
| Target Date  | October 31, 2020 |
| Slack        | [#wg_dogfood_plan](https://gitlab.slack.com/archives/C0188LMC2LQ) (internal only) |
| Google Doc   | [Dogfood Plan Working Group Agenda](https://docs.google.com/document/d/1x7WZd_ilH9N4TvbUwdFKw2hcdlQ80SaN2CmHdNwnEzI/edit) (internal only) |

## Business Goal

This working group will integrate new Plan stage features in GitLab’s own agile planning process. Additionally, there may be some existing features we can consume. We would start with the Plan stage group itself, asking the people who are responsible for the features to implement their usage locally. Assuming success we might roll this out further across R&D (other stages) in a future iteration. The benefits we hope to achieve are:

* Our current process is not standardized and some teams are probably planning releases in a sub-optimal (not efficiently, transparently, async, etc) because they are not benefitting from what other teams have figured out works well
* Dogfooding
    * If we’re going to standardize on a tool it should be our own product
    * It’ll make the product better for users/customers
* This will reiterate to the team, users, and customers how invested we are in our own planning feature set while GitLab also integrate with other planning tools because they are entrenched in customer organizations

## Exit Criteria

* [ ] Determine feature within plan that will be used for dogfooding
    * [Iterations](https://docs.gitlab.com/ee/user/group/iterations/)
* [ ] Finalized OKR wording and update issues
    * [FY21-Q3 Engineering Division OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8303)
    * Development: [KR: Project planning & Portfolio management](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8429)
    * UX: [KR: Dogfood the Iterations feature in Plan to decrease the time issues spend in workflow::design](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8436)
    * Quality: [KR: Dogfood project planning and project management feature for proactive quality test-planning processes](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8369)
    * PM: [KR: Dogfood iterations](https://gitlab.com/gitlab-com/Product/-/issues/1406)
* [ ] An epic with issues that are needed to effectively use iterations within the greater GitLab organization
* [ ] A recorded YouTube demo video suitable for teaching other stages how to modify their processes to use iterations

## Roles and Responsibilities

| Working Group Role          | Person            | Title                        |
|-----------------------------|-------------------|------------------------------|
| Facilitator                 | Tim Zallmann      | Director of Engineering      |
| Development Department Lead | Donald Cook       | Frontend Engineering Manager |
| UX Department Lead          | Mike Long         | Product Design Manager       |
| Quality Department Lead     | Ramya Authappan   | Quality Engineering Manager  |
| PM Division Lead            | Justin Farris     | Group Manager, PM            |
| Member                      | Gabe Weaver       | Senior Product Manager       |
| Member                      | Keanon O'Keefe    | Senior Product Manager       |
| Member                      | Mark Wood         | Senior Product Manager       |
| Member                      | John Hope         | Backend Engineering Manager  |
| Member                      | Jake Lear         | Backend Engineering Manager  |
| Executive Sponsor           | Eric Johnson      | EVP of Engineering           |
