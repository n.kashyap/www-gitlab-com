---
layout: markdown_page
title: "Diversity, Inclusion & Belonging Engineering Initiatives"
description: "Learn more about GitLab Diversity, Inclusion & Belonging Engineering Initiatives."
canonical_path: "/company/culture/inclusion/engineering-initiatives/"
---

This page is in [draft](/handbook/values/#everything-is-in-draft). We are working to build out this page and gather all the running initiatives/partnerships in Engineering. 

If you're looking for information on the current initiatives in Engineering please see: 

* [Upstream Diversity Working Group](/company/team/structure/working-groups/upstream-diversity/)
* [Minorities in Tech Mentoring program](/company/culture/inclusion/erg-minorities-in-tech/mentoring/)
* [FY21-Q3 KR: 75% of Managers in the Engineering Division complete DIB training](75% of Managers in the Engineering Division complete DIB training => 72.45%)
* [Team Member Resource Groups](/company/culture/inclusion/erg-guide/#how-to-join-current-tmrgs-and-their-slack-channels)
* [Diversity, Inclusion and Belonging GitLab initiatives](/company/culture/inclusion/#diversity-inclusion--belonging-mission-at-gitlab)
