---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-09-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 134       | 10.66%          |
| Based in EMEA                               | 335       | 26.65%          |
| Based in LATAM                              | 20        | 1.59%           |
| Based in NORAM                              | 768       | 61.10%          |
| **Total Team Members**                      | **1,257** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 876       | 69.69%          |
| Women                                       | 381       | 30.31%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,257** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 155       | 67.98%          |
| Women in Management                         | 73        | 32.02%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **228**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 73        | 76.04%          |
| Women in Leadership                         | 23        | 23.96%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **96**    | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 436       | 81.04%          |
| Women in Engineering                        | 102       | 18.96%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **538**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.28%           |
| Asian                                       | 47        | 6.53%           |
| Black or African American                   | 19        | 2.64%           |
| Hispanic or Latino                          | 39        | 5.42%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 4.17%           |
| White                                       | 444       | 61.67%          |
| Unreported                                  | 139       | 19.31%          |
| **Total Team Members**                      | **720**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 7.55%           |
| Black or African American                   | 4         | 1.89%           |
| Hispanic or Latino                          | 8         | 3.77%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.25%           |
| White                                       | 139       | 65.57%          |
| Unreported                                  | 36        | 16.98%          |
| **Total Team Members**                      | **212**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 6.12%           |
| Black or African American                   | 2         | 1.36%           |
| Hispanic or Latino                          | 4         | 2.72%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 4.08%           |
| White                                       | 91        | 61.90%          |
| Unreported                                  | 35        | 23.81%          |
| **Total Team Members**                      | **147**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 11.84%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 5.26%           |
| White                                       | 48        | 63.16%          |
| Unreported                                  | 15        | 19.74%          |
| **Total Team Members**                      | **76**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.16%           |
| Asian                                       | 115       | 9.15%           |
| Black or African American                   | 30        | 2.39%           |
| Hispanic or Latino                          | 65        | 5.17%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.08%           |
| Two or More Races                           | 38        | 3.02%           |
| White                                       | 712       | 56.64%          |
| Unreported                                  | 294       | 23.39%          |
| **Total Team Members**                      | **1,257** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 55        | 10.22%          |
| Black or African American                   | 9         | 1.67%           |
| Hispanic or Latino                          | 26        | 4.83%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.19%           |
| Two or More Races                           | 15        | 2.79%           |
| White                                       | 305       | 56.69%          |
| Unreported                                  | 127       | 23.61%          |
| **Total Team Members**                      | **538**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 15        | 6.58%           |
| Black or African American                   | 2         | 0.88%           |
| Hispanic or Latino                          | 6         | 2.63%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.07%           |
| White                                       | 143       | 62.72%          |
| Unreported                                  | 55        | 24.12%          |
| **Total Team Members**                      | **228**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.42%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.04%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 4         | 4.17%           |
| White                                       | 57        | 59.38%          |
| Unreported                                  | 24        | 25.00%          |
| **Total Team Members**                      | **96**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 16        | 1.27%           |
| 25-29                                       | 204       | 16.23%          |
| 30-34                                       | 348       | 27.68%          |
| 35-39                                       | 285       | 22.67%          |
| 40-49                                       | 277       | 22.04%          |
| 50-59                                       | 111       | 8.83%           |
| 60+                                         | 16        | 1.27%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,257** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
