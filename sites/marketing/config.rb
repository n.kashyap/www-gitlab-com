#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

monorepo_root = File.expand_path('../..', __dir__)

# hack around relative requires elsewhere in the shared code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'marketing'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

require 'generators/direction'
require 'generators/releases'
require 'generators/org_chart'
require 'extensions/only_debugged_resources'
require 'extensions/partial_build'
require 'extensions/destination_path_regexes_filter'
require 'extensions/listen_monkeypatch'
require 'lib/code_owners'
require 'lib/homepage'
require 'lib/mermaid'
require 'lib/plantuml'

#----------------------------------------------------------
# Global config (not specific to development or build mode)
#----------------------------------------------------------

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false
page '/404.html', directory_index: false

# Don't render or include the following into the sitemap
ignore '/direction/*' unless ENV['PRIVATE_TOKEN']
ignore '/frontend/*'
ignore '/templates/*'
ignore '/includes/*'
ignore '/upcoming-releases/template.html'
ignore '/releases/template.html'
ignore '/releases/gitlab-com/template.html'
ignore '/company/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore 'source/job-families/check_job_families.py'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
ignore '/sites/*'
ignore '/jobs/apply/interim_landing_page.html.haml'
ignore '/jobs/apply/job-listing/template.html'
ignore '/experiments/*'

# Ignore markdown content used to generate devops comparison pages. TODO - Migrate all these pages to data
ignore '/devops-tools/anchore/*'
ignore '/devops-tools/artifactory/*'
ignore '/devops-tools/asana/*'
ignore '/devops-tools/awsopsworks/*'
ignore '/devops-tools/azure_devops/index.html.md.erb'
ignore '/devops-tools/azuremonitor/*'
ignore '/devops-tools/bamboo/*'
ignore '/devops-tools/bitbucket/*'
ignore '/devops-tools/broadcom-apm/*'
ignore '/devops-tools/buildkite/*'
ignore '/devops-tools/checkmarx/*'
ignore '/devops-tools/circleci/index.html.md'
ignore '/devops-tools/cloudbuild/*'
ignore '/devops-tools/codefresh/*'
ignore '/devops-tools/codeship/*'
ignore '/devops-tools/crucible/*'
ignore '/devops-tools/datadog/*'
ignore '/devops-tools/dynatrace/*'
ignore '/devops-tools/electricflow/*'
ignore '/devops-tools/gerrit/*'
ignore '/devops-tools/github/index.html.md.erb'
ignore '/devops-tools/harness/*'
ignore '/devops-tools/hcl-urbancode-deploy/*'
ignore '/devops-tools/heroku/*'
ignore '/devops-tools/ibm-apm/*'
ignore '/devops-tools/jenkins/index.html.md.erb'
ignore '/devops-tools/jfrog/*'
ignore '/devops-tools/jira/*'
ignore '/devops-tools/launchdarkly/*'
ignore '/devops-tools/micro-focus-apm/*'
ignore '/devops-tools/microfocus-alm-octane/*'
ignore '/devops-tools/microfocus-fortify/*'
ignore '/devops-tools/misc/*'
ignore '/devops-tools/missing/*'
ignore '/devops-tools/nagios/*'
ignore '/devops-tools/new-relic/*'
ignore '/devops-tools/new-template/*'
ignore '/devops-tools/openshift/*'
ignore '/devops-tools/prisma_cloud/*'
ignore '/devops-tools/redmine/*'
ignore '/devops-tools/SaltStack/*'
ignore '/devops-tools/semmle/*'
ignore '/devops-tools/spinnaker/*'
ignore '/devops-tools/splunk/*'
ignore '/devops-tools/synopsys/*'
ignore '/devops-tools/teamcity/*'
ignore '/devops-tools/trello/*'
ignore '/devops-tools/veracode/*'
ignore '/devops-tools/victorops/*'

# Extensions
activate :syntax, line_numbers: false
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end
activate :partial_build # Handle splitting of files across CI jobs for paths handled by top-level build

# Blog and Release extensions
unless ENV['SKIP_BLOG']
  activate :blog do |blog|
    blog.name = 'blog'
    # This will add a prefix to all links, template references and source paths
    blog.prefix = 'blog'
    blog.sources = 'blog-posts/{year}-{month}-{day}-{title}.html'
    blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
    blog.layout = 'post'
    # Allow draft posts to appear on all branches except master (for Review Apps)
    blog.publish_future_dated = true if ENV['CI_COMMIT_REF_NAME'].to_s != 'master'

    blog.summary_separator = /<!--\s*more\s*-->/

    blog.custom_collections = {
      categories: {
        link: '/categories/{categories}/index.html',
        template: '/category.html'
      }
    }
  end

  activate :blog do |blog|
    blog.name = 'releases'
    # This will add a prefix to all links, template references and source paths
    blog.prefix = 'releases'
    blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
    blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
    blog.layout = 'post'
    # Allow draft posts to appear on all branches except master (for Review Apps)
    blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

    blog.summary_separator = /<!--\s*more\s*-->/

    blog.custom_collections = {
      categories: {
        link: '/categories/{categories}/index.html',
        template: '/category.html'
      }
    }
  end
end

# Begin proxy resources
org_chart = OrgChart.new
proxy '/company/team/org-chart/index.html', '/company/team/org-chart/template.html', locals: { team_data_tree: org_chart.team_data_tree }, ignore: true

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}/index.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }
end

data.features_v2.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}/index.html", "/templates/comparison-v2.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }
end

data.devops_tools.each do |devops_tool, files|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  unless files.decision_kit.nil?
    proxy "/devops-tools/#{file_name}/decision-kit/index.html", "/templates/comparison_detailed.html", locals: {
      key_one: devops_tool,
      key_two: 'gitlab_ultimate'
    }
  end

  files.extra_content&.pages&.each do |page|
    proxy "/devops-tools/#{file_name}/#{page.path}/index.html", "/templates/comparison_content.html", locals: {
      key_one: devops_tool,
      key_two: 'gitlab_ultimate',
      content: page.content
    }
  end
end

# Analyst reports
data.analyst_reports.each do |report|
  next unless report.url

  proxy "/analysts/#{report.url}/index.html", '/analysts/template.html', locals: {
    report: report
  }, ignore: true
end

# Category pages for /stages-devops-lifecycle
data.categories.each do |key, category|
  next unless category.body && category.maturity && (category.maturity != "planned") && category.marketing

  proxy "/stages-devops-lifecycle/#{key.dup.tr('_', '-').downcase}/index.html", '/stages-devops-lifecycle/template.html', locals: {
    category: category,
    category_key: key
  }, ignore: true
end

# Event pages
data.events.each do |event|
  next unless event.url

  proxy "/events/#{event.url.tr(' ', '-')}/index.html", '/events/template.html', locals: {
    event: event
  }, ignore: true
end

# Conversion pages under /forms
data.forms.each do |form|
  proxy "/forms/#{form.url.tr(' ', '-')}/index.html", '/forms/template.html', locals: {
    form: form
  }, ignore: true
end

# Webcast pages
data.webcasts.each do |webcast|
  proxy "/webcast/#{webcast.url.tr(' ', '-')}/index.html", '/webcast/template.html', locals: {
    webcast: webcast
  }, ignore: true
end

# Reseller page
data.resellers.each do |reseller|
  proxy "/resellers/#{reseller.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').downcase.to_s.tr(' ', '-')}/index.html", '/resellers/template.html', locals: {
    reseller: reseller
  }, ignore: true
end

# Release Radars /webcast/monthly-release
data.release_radars.each do |release_radar|
  proxy "/webcast/monthly-release/#{release_radar.name.tr(' ', '-').downcase}/index.html", '/webcast/monthly-release/template.html', locals: {
    release_radar: release_radar
  }, ignore: true
end

# create new job listing instance when /jobs/apply reached
proxy "/jobs/careers/index.html", '/jobs/apply/template.html', ignore: true

# Proxy case study pages
data.case_studies.each do |filename, study|
  proxy "/customers/#{filename}/index.html", '/templates/case_study.html', locals: { case_study: study }
end

# See https://gitlab.com/gitlab-com/infrastructure/issues/4036
proxy '/development/index.html', '/sales/index.html'
# End proxy resources

#----------------------------------------------------------
# End global config (not specific to development or build mode)
#----------------------------------------------------------

# Development-specific config
configure :development do
  # Reload the browser automatically whenever files change
  activate :livereload unless ENV['ENABLE_LIVERELOAD'] != '1'

  # External Pipeline
  unless ENV['SKIP_EXTERNAL_PIPELINE']
    # NOTE: This only applies to 'development' mode.  For local builds, use the `rake build:*` tasks
    activate :external_pipeline,
             name: :frontend,
             command: "cd #{monorepo_root} && #{monorepo_root}/source/frontend/pipeline.sh #{build? || environment?(:test) ? '' : ' --watch'}",
             source: "#{monorepo_root}/tmp/frontend",
             latency: 3
  end

  activate :autoprefixer do |config|
    config.browsers = ['last 2 versions', 'Explorer >= 9']
  end

  activate :minify_css
  activate :minify_javascript
end

# Build-specific config
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true

  # Begin proxy resources

  # create routes to job page using job-listing template

  # Currently commenting out the iteration below on jobs
  # When ready to repost jobs to the jobs page please comment
  # back in lines below

  # Gitlab::Homepage::Jobs::JobsListing.new.jobs.each do |job|
  #   proxy "/jobs/apply/#{job[:title].downcase.strip.tr(' ', '-').gsub(/[^\w-]/, '')}-#{job[:id]}/index.html",
  #         "/jobs/apply/job-listing/template.html",
  #         locals: { job: job },
  #         ignore: true
  # end

  # Populate the direction and releases pages only if INCLUDE_GENERATORS is true
  # That will help shave off some time of the build times when they are not needed
  if ENV['INCLUDE_GENERATORS'] == 'true'

    # Direction page
    if ENV['PRIVATE_TOKEN']
      content = {}
      direction = Generators::Direction.new

      wishlist = direction.generate_wishlist # wishlist, shared by most pages
      direction_all_content = direction.generate_direction(Generators::Direction::STAGES, Generators::Direction::INCLUDE_EPICS) # /direction/*
      direction_dev_content = direction.generate_direction(Generators::Direction::DEV_STAGES, Generators::Direction::INCLUDE_EPICS) # /direction/dev/
      direction_ops_content = direction.generate_direction(Generators::Direction::OPS_STAGES, Generators::Direction::INCLUDE_EPICS) # /direction/ops/
      direction_enablement_content = direction.generate_direction(Generators::Direction::ENABLEMENT_STAGES, Generators::Direction::INCLUDE_EPICS) # /direction/ops/

      Generators::Direction::STAGES.each do |name|
        # Fetch content for per-team pages
        skip = %w[release verify package] # these do not have a team page
        content[name] = direction.generate_direction(%W[#{name}], Generators::Direction::INCLUDE_EPICS) unless skip.include? name # /direction/name/
      end

      stage_contribution_content = direction.generate_contribution_count(data.stages) # /direction/maturity/
      stage_velocity = direction.generate_stage_velocity(data.stages) # /direction/maturity/

      milestones = direction.generate_milestones # /releases/gitlab-com

      # Set up proxies using now-fetched content for shared pages
      proxy '/upcoming-releases/index.html', '/upcoming-releases/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/paid_tiers/index.html', '/direction/paid_tiers/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/releases/gitlab-com/index.html', '/releases/gitlab-com/template.html', locals: { milestones: milestones }, ignore: true
      proxy '/direction/dev/index.html', '/direction/dev/template.html', locals: { direction: direction_dev_content }, ignore: true
      proxy '/direction/ops/index.html', '/direction/ops/template.html', locals: { direction: direction_ops_content }, ignore: true
      proxy '/direction/enablement/index.html', '/direction/enablement/template.html', locals: { direction: direction_enablement_content }, ignore: true
      proxy '/direction/maturity/index.html', '/direction/maturity/template.html', locals: { stage_contributions: stage_contribution_content, stage_velocity: stage_velocity }, ignore: true
      proxy '/direction/kickoff/index.html', '/direction/kickoff/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/moonshots/index.html', '/direction/moonshots/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/direction/index.html', '/direction/template.html', locals: { wishlist: wishlist }, ignore: true

      Generators::Direction::STAGES.each do |name|
        # And for team pages
        skip = %w[release verify package] # these do not have a team page
        proxy "/direction/#{name}/index.html", "/direction/#{name}/template.html", locals: { direction: content[name], wishlist: wishlist }, ignore: true unless skip.include? name
      end
    end

    ## Releases page
    releases = ReleaseList.new
    proxy '/releases/index.html', '/releases/template.html', locals: {
      list: releases.generate,
      count: releases.count
    }, ignore: true
  end
  # End proxy resources
end
