---
layout: markdown_page
title: "Creating and Publishing Technical Marketing Demo Videos"
---

## Creating and Publishing 

This is the SSoT for the steps to creating and publishing video demos. These are designed to show up in our issue templates. There are several types of demo issue templates but this is what they have in common (with the exception of some specific publish locations details which need to be adjusted per issue template).

```markdown
### Video should include:
* [ ] Tell one story across the whole video (have one key take away).
* [ ] What is being shown
* [ ] Why it matters to the audience
* [ ] Show it in action

### Acceptance Criteria
* [ ] Length: \<\= X mins <!-- (3 for feature, market requirement, and differentiator videos, 7 for overview) -->
* [ ] Resolution: 1920 x 1080
* [ ] Casual/Comfortable tone throughout video
* [ ] Introduce yourself and be personable. The video should show you in the beginning and then transition to a demo
* [ ] Feel free to add your own personality. Build a brand.

### Production
* [ ] Develop Demo Script - in partnership w/ PMM, PM, and other stakeholders
   * [ ] Use [this template](https://docs.google.com/document/d/1Cg-8kL71lhoGqiNguh7dDwk4LeofXPKjOsQD0gAWtpo/edit) to encourage collaboration. Link your copy of it in this issue.
* [ ] Configure demo scenario
* [ ] Record Demo

### Post-Production
* [ ] Perform voice overs as necessary
* [ ] When switching subjects in the video, add a section divider (use the "Gradient - Edge" from the FinalCutPro title library)
* [ ] Add cross-dissolves for transitions
* [ ] Add unique style to different parts of video to keep audience engaged

### Publishing
* [ ] Publish demo video to youtube **"GitLab"** channel, playlists: "Learn@GitLab".
   * [ ] Position video in Learn@GitLab playlist within the appropriate use case.
   * [ ] Add CTA's to the end of the new video and fix any other ones so the chain stays intact. See [adding CTA's to Learn videos](/handbook/marketing/product-marketing/technical-marketing/howto/add-ctas-to-learn-videos.html) for more details.
   * [ ] Create attractive video thumbnails. See [thumbnail guide](https://louisem.com/198803/how-to-youtube-thumbnails) for more details.
* [ ] Publish demo video to Learn Demo page at https://about.gitlab.com/learn/. Details on [asset inventory page](/handbook/marketing/product-marketing/asset_inventory/)
* [ ] Update the Resources section of the Use Case Resource page, which you can find on the [Usecase GTM page](/handbook/marketing/product-marketing/usecase-gtm/#use-case-gtm-areas-of-interest). Add under "Other Videos" at the bottom of page unless it fits within a Market Requirement.
* [ ] Add the final asset to the [SM Inventory Data spreadsheet](https://docs.google.com/spreadsheets/d/1W5oAlbPV610-ylM7LWv_zc6bEqQK9dI0H9Hrn2f6Jwc/edit#gid=0)
* [ ] Add demo video to [GDrive Demo Folder](https://drive.google.com/drive/u/0/folders/1AWGh_v8Gn26RYhPYmc4jWor-RgqvngRZ) in the appropriate folder.
* [ ] Announce new video (with link) to Slack channel #marketing
```

## Issue Templates the above is used in

* Feature Demo
* Use Case Differentiator Demo
* Use Case Market Requirement Demo
* Use Case Overview Demo
