---
layout: handbook-page-toc
title: "SEO Content Manual"
description: "The aim of this manual is to help create content that provides use and value to both searchers and search engines."
canonical_path: "/handbook/marketing/growth-marketing/inbound-marketing/seo-content-manual/"
---

## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## Introduction and Purpose

The aim of this manual is to help create content that provides use and value to both searchers and search engines.

SEO optimising content during the initial production phase ensures maximum impact from launch in terms of attracting qualified traffic and delivering to them valid content that matches the searchers query in terms of both topic and intent.

## Goal

Ideally, marrying both content writing skills with SEO helps to meet the E.A.T (Expertise, Authority, Trust) requirements of search engines such as Google. To do so two elements are required:

1. The content created has to be relevant, informative and engaging
2. The search engine needs to be able to discern what the content is about, what information it provides to the user, and most importantly, whether it provides better information than those you are topically competing with.

The goal of this unification is to:

1. Rank as high as possible in SEARCH results for specific topics and related terms. To drive qualified traffic - not just general traffic
2. To engage and create brand trust and return visits
3. To convince readers to take specific actions and to convert
4. To create an optimised internal linking structure which indicates relevance, relationships between content pieces as well as funnels for pushing traffic through general buyer journey stages

Ideally, content production takes a topic first approach. Meaning, target topics are identified which is followed by keyword research which maps keywords to the identified topics.

## Digital Content Publication SEO Checklist

Creating valuable content means not just publicizing useful and attractive information, but also assuring that the information can be found. The following is a checklist which goes through each step required to make sure that all content is fully optimised before digital publication.

### Quick Summary

1. Include your keyword in the URL
2. Keep URLs as short as possible
3. Front load your keyword in your Title Tag
4. Embed Title Tag modifiers
5. Use your keyword (or variants) in the subheadings (H1,H2,H3)
6. Use your keyword as close to the beginning of the first paragraph of text as possible.
7. Optimise your images (image Alt Tags)
8. Use semantic keyword terms within the copy
9. Use external links to authoritative sites, but ensure any external links open in a new window so as not to end the session - try to ensure that any external links are placed low down in the copy, after any prioritised or strategic links.
10. Use internal links and use keyword rich anchor text which indicates the pages relationship. Also target pages which funnel traffic through buyer stages.
11. Avoid long sentences, as this also puts readers off. If a sentence goes over two and a half sentences in length, then it should most likely be broken into two separate sentences. Keep direct and to the point.
12. Strategically place CTAs within the page, not just header and footer.

## Double Check the Following Prior to Publication

### Findable content: does the content include?

- Primary keyword target in URL
- A H1 tag (with keyword target)
- At least 2 H2 tags (with keyword target or keyword variants)
- Placement of the target keyword as close to the beginning of the first sentence on the first paragraph as possible.
- The primary [keyword density](https://blog.alexa.com/keyword-density/) within the copy is about 2% (guideline, not a rule).
- Metadata that matches the intent of the page content, including title tag, meta description and keywords properly placed
- Inclusion of semantic terms where possible
- Links to other related content (internal/external)
- Alt tags for images (with related keyword/variants)

### Readable content: does the content include?

- An inverted pyramid writing style
- Chunking
- Bullet points where necessary
- Numbered lists where necessary
- Follow a style guide for consistency in terminology etc

### Understandable content: does the content include?

- An appropriate content type (text, video, etc).
- Reflection that you used the buyer persona
- Context
- Respect for the audiences reading level
- Present an old idea in a new way

### Actionable content; does the content include?

- A call to action (strategically placed and matching the searchers intent)
- A specific solution to a pain point (preferably identified using buyer persona data)
- A place to comment
- An invitation to share
- Links to related content
- A direct summary of what to do

## **Identifying Customer Language to Use in Keyword Targeting**

When identifying what key terms to target within the keyword research process it is important to ensure the use of valid and current customer terminology for a specific target topic.

Over time, the way topics or categories are discussed by professionals or industry users can alter, expand, contract and evolve. As such, it is essential to remain current with the customers’#39; language.

At GitLab we have access to valuable resources that help inform, validate and evolve the targeting process.

A brief overview of some of the most valuable resources and assets that are utilized as part of the cyclical keyword planning stage.

1. [**Strategic Marketing**](https://about.gitlab.com/handbook/marketing/product-marketing/#some-key-resources) - linking with a member of the team and accessing their wealth of information and research resources provides exceptional insight into our customers allowing for up to date targeting using customer focus language and needs.

1. [**Key personas &amp; pain points**](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/) - user persona research assists in the identification of possible keyword targets for a specific audience based on their requirements, motivations and pain points. As a resource, this is excellent for establishing problem solving keyword targets for specific segments.

1. [**Chorus**](https://chorus.ai/) - provides information and insight based on customer conversations, and most importantly, using their own language. The filtering and analysis of discussion transcripts inform keyword targeting research and assist in the identification of current terms. An additional benefit is the capability of contextual analysis of specific terms.

Using data and research to understand the evolving needs of the customer as well as their preferences for terminology allow for the development of targeted strategies that maximise impact.

The incorporation of this information into the planning stage ensures a focus on user intent as well as the delivery of highly relevant solution based content.

## Explanation of General Terms

### Keyword List

Keyword lists are provided during the research stage based on targeting parameters and requirements. Each list is researched and scored to identify targeting opportunities based on multiple criteria such as search volume, competitiveness etc. The keywords are then tagged based on any intent modifiers in order to segment targeting by intent stage and to maximise impact.

When a keyword list has been used for a project stage, it is important to highlight any terms used as the primary page target within the document. This helps to ensure no retargeting of the same term takes place and avoid any keyword cannibalization issues down the line.

Keyword lists are updated frequently, and identifying already targeted terms helps improve the process and also to initiate keyword tracking within campaign structures when required.

### Keyword Grouping

This is where overlapping keywords are identified within a keyword list and grouped to target on a singular page to maximise organic traffic for a topic.

This helps ensure that multiple pages are not produced for a series of keywords which can then end up competing for each other for traffic.

**Resource:** [https://moz.com/learn/seo/multiple-keywords-video](https://moz.com/learn/seo/multiple-keywords-video)

### Semantic Keywords

Semantic keywords should also be applied to a page where possible, these are terms that google likes to see present based on the target topic and highlights how extensively a topic is covered based on content which currently ranks in SEARCH.

Lists of semantic terms are provided to the content writer during the research phase along with keyword lists and trending topic analysis.

**Resource:** [https://backlinko.com/hub/seo/lsi](https://backlinko.com/hub/seo/lsi)

### Natural Language

It is important to always place natural language at the forefront of page optimization. Key terms are used when appropriate and should never be placed in circumstances where they do not sound natural (keyword stuffing).

### Trending Topics and Related Questions

During the research phase, keyword lists are provided to content writers along with trending topic lists and lists of trending questions.

- Trending topics allow for the writers to research the top 10 ranking articles for their target topic.
- Trending questions are identified questions that pop up most frequently, and can be used within articles (use exact wording) and to provide comprehensive sought after answers.

### Searcher Intent

Each webpage and the content that is placed on it should be focused on user intent. Your content will vary depending on what stage of a customer journey your reader is at. And so should your keywords.

- Informational Intent
- Commercial Intent
- Navigational Intent
- Transactional Intent

When targeting a specific keyword term, the SEO team will provide a data set of keywords which fall under this overall target. The keyword list will be categorized so as that the list can be filtered into the three primary targets of intent.

This will allow for the mapping of individual articles which can be produced to tackle a customer at each buyer stage.

### Gap Analysis

This process has two elements, internal and external.

- **Internal:** what content do we have internally that already covers this topic - this ensures we do not create keyword cannibalization issues as well as help to optimise a content clustering strategy approach in terms of linking.

- **External:** What content do our competitors have that cover this topic, what is performing well for this in search, what format does it take? We use this information to inform our own production.

### Pyramid Style Writing Technique

As readers tend to drop off according to the 80/20 rule (80% drop off after the initial 20% of page content) it can be good practice to top load the page copy with content that includes a solution to the searchers pain point. Addressing the issue here, and then moving into broader areas of the topic following this helps to ensure that the solution is provided while attention is still maintained.

Content should be formatted for scan ability; most site visitors have a very limited attention span and therefore it is best to use an inverted pyramid style. This refers to cramming all the important points into the beginning of an article or piece and then broadening as it moves on. This way the most beneficial marketing points have a higher chance of impact within this limited time frame even if the recipient bounces.

It can also be good practice to include a CTA following this to help promote action related to the searchers pain point, and the solution which is being provided.
**Resource:** [https://yoast.com/inverted-pyramid/](https://yoast.com/inverted-pyramid/)

### Sentence Length

Sentences within articles will vary in length. Accumulated short sentences make a text seem choppy and hard to digest, overly long sentences can seem overly complicated and dull to a reader. Therefore, a healthy mixture of both is favorable. However, shorter, clearer and more impactful sentences are preferable. (Short sentences < 17 words / long sentences > 35 words)

 Any sentence which is more than 2-3 sentences long on a word document can generally be broken into two separate sentences. The 2.5-3 lines long category is generally above and beyond the recommended limit except in exceptional circumstances.

### Use Active Text Vs Passive Text

Avoid use of passive text. Overuse of passive text is seen as non-persuasive and also as poor writing. It allows the object of an action to be the subject of the sentence. On average this should be kept below 20% of the overall text, generally depending on the type of piece that is to be generated.

With the active voice, you learn **'who' or 'what' is responsible for the action at the beginning of the sentence.** In other words, when the subject acts, the verb is active.

Active voice tends to make text shorter, clearer and more impactful - most importantly, active text is more persuasive:

- Passive voice - Groceries were bought by my parents for my sister's birthday party.

- Active voice - My parents bought groceries for my sister's birthday party.

## On-page Factors to Consider

### Title Tag

Your meta title tag should be accurate and descriptive. It shows as the heading above your meta description on the search results page and as such strongly impacts the click through rate. The text should, in general, be no longer than 56 characters (though this is finally determined by pixel length) and should contain key terms related to the page.

The text between your title tags is often displayed on search engine results pages (SERPs). These tags are therefore useful in both SEO and for social sharing.

### Meta Description Tag

Your meta description tag is a unique, and clear description of what is featured on the page and appears below a page link/title on SERPs. A good meta description can increase your click-through rate.

- 140-160 characters long
- Include your keyword
- Add a call-to-action
- Avoid duplication
- Make them meaningful

### Heading Tags (H1, H2)

Headers are the things which summarise the pages you create for both consumers and search engines, as well as being important stylistically.

All content, barring news articles, should consist of a H1 and subsequent H2 breaking up each section. This is for SEO and reader scan ability.

 Keywords should be used in H1 and also as close as possible to the beginning of the first sentence of paragraph 1. This is as the Google bots scan pages from the top down when deciding how relative they are.

`<h1\></h1\>` This tag is your main page heading – it's from this that Google will take its first impression of your page's relevance to a search query. It is important for page relevance that these contain the information you wish to rank for, such as your target keywords. Though there is no definitive limit, best practice dictates that you should attempt to keep your titles unique, between 60 and 80 characters, and with keywords near the beginning.

`<h2\></h2\>` are also good for use as stand-alone subheadings for breaking up content and allowing for ease and speed of understanding. Readers will at times scan the content for relative sections, such headers quickly identify content they may require.

`<h3\></h3\>` This is your sub-sub and its formatting should denote its place as a paragraph leader, or stand-out line. Only use a H3 if necessary, there are arguments stating that using H3s can detract or confuse crawlers when analysing the page topic of a specific page if it veers too far from the original content target (subcategory of a subcategory).

### Image Alt Tag

Your image tag provides an explanation of images to search engines, which can be used in conjunction with various modifiers to position it according to your needs. This is an important factor of on-page optimization.

### Text Link (hyperlink)

Used for your on page links (your hypertext underlines), the anchor text should give easily inferred signals about what awaits them on the destination page. This is good for both the searcher and search engines, as it indicates to the search engine the relationship between the current page and the one that the link is connected to.

### On-Page Linking (internal and external)

**Internal Links** - whenever publishing a new piece of content, it is important to include links to 2-5 other pages within your site. This process is ideal for identifying relationships between content groups/ clusters or for mapping a buyer journey so as to funnel traffic to the next stage of conversion.

Be sure to use keyword rich anchor text for your internal links as these help indicate context to both the searcher and search engines.

**External Links** - external links to trusted websites are an important means of showing search engines that your content is well-referenced, trustworthy and backed up. However, be careful where you place these links, too close to the top of the page and you could be funneling traffic away from the site.

Also, be careful not to link to content that is covering the very same topic, as searchers may travel to the external page and be drawn into their buyer funnel as it provides information on the same query/intent.

## Optimisation Strategies

### Historical Optimisations

Not only is historical blog optimization a way to get more out of the content you already have, it's also a way to get a leg up on such a competitive landscape by keeping your content evergreen and delivering even more value to the people reading it.

This tactic is so effective simply because the content has been indexed for much longer and already has some authority in Google, which in turn will make it easier for your optimized post to gain traction and start moving up the ranks.

Before you start updating random articles, though, you should take a strategic approach to get the biggest impact and identify critical articles to optimize.

#### How to identify which articles to optimize

When it comes to optimizing your past articles, the primary focus is on four core areas:

1. [Low-converting articles](#low-converting-articles)
2. [High bounce rate articles](#high-bounce-rate-articles)
3. [Highly-indexed, low-clicked articles from the Search Engine Results Page (SERP)](#high-indexed-low-clicked-articles)
4. [Page one ranking opportunity articles](#page-one-ranking-opportunity-articles)

When not focusing on individual page optimisations, it is best to categorize and group pages based on the above and to optimise each group within a project setting.

##### Low-converting articles

You spend so much time, effort, and money creating quality blog articles. Ideally, you're getting a ton of traffic to your site because of it. Traffic is great, but we need people to contact you and get in your sales pipeline.

By focusing on low converting articles, you'll be able to make small updates that can help drive big results

##### High bounce rate articles

Having a higher bounce rate with your blog articles than your website pages is common because most people are looking to find specific educational information and, once they have read it, they tend to leave your site. But that does not mean you settle for high bounce rates from your articles! If these pages have good dwell time from the readers it means the content is delivering the required information and the reader is engaged. But we would look for a means of driving that traffic further into the funnel and reduce the bounce rate. If the time on page is low, then most likely the content did not align with the intent of the searcher and requires analysis.

##### High-indexed, low-clicked articles

Articles that receive a significant number of impressions in search engines but not a lot of clicks through to your site is a perfect optimization opportunity to dramatically increase your traffic. By making some minimal tweaks to page titles and meta descriptions, you can encourage more searchers to click through to your site.

##### Page one ranking opportunity articles

These are articles that are currently on pages two or three of Google's search results. You're close to seeing a big traffic increase if you can improve the content of those articles and get them on page one of the SERP. Moving to this position greatly increases the CTR %.

### Individual Page Optimisations (Unique Tasks)

Individual page optimization projects (post-production) would differ in approach as each undertaking would require the analysis of the data of each individual page to be optimized.

As such, using the checklist document discussed would be beneficial for the inclusion of any missing elements, but would also include:

- Analysis of the page performance metrics to identify the underlying performance issues
- Analysis of the page structure to identify any UX issues
- Analysis of on-page SEO factors
- Gap analysis of competing content

## Content Clustering Strategy

### Process and Application

A content clustering strategy uses internal linking structures and topic modelling to indicate to search engine crawlers the relationship between content pages. Using this approach helps to provide such crawlers with an overview of the site's topic ownership and expertise on certain subjects and as such boost search performance.

It is a means of organising and archiving content by subject matter and provides an intentional, organized and structured approach to SEO content production.

### Primary Elements

1. Pillar page - this page is a broad piece of content that covers your target topic, it also acts as the primary hub for your cluster. Like a hub in the centre of an old cart wheel with multiple spokes providing support.
2. Cluster pages - these pages are internally linked, to and from, your topic pillar page. Each page provides an in-depth look at specific subtopics related to the primary topic target.
3. Internal links - the links act as the bridge that ties all the elements together.

The linking structure indicates to search engines what the relationship between all elements of the structure are, and also the level of expertise and knowledge that a site can provide on a given topic.

Google uses its Google RankBrain algorithm to evaluate pages for ranking and determine what results to provide for a given search query based on intent.

Utilizing this approach helps positively influence factors that this algorithm uses when evaluating both quality and authority of a site/topic.

### Executing a Content Clustering Strategy

1. Identify goals and execute keyword research based on the target topic. Execute gap analysis on performing competitive content to identify what has traction within search based on intent type.
2. Execute internal content audit and identify existing live assets which can be included within the cluster. This has two main impacts, firstly it ensures we are not creating overlapping content and, secondly including live content that has traction speeds up the ranking process.
3. Establish a pillar page to tie these assets together and formalise your cluster and provide a hub for your links to and from these pages. At times it is possible to expand upon a suitable existing page to create your pillar page, other times it may be necessary to create a new page to satisfy this function.
4. Establish your internal linking structure to tie all elements together. All cluster pages should link to the pillar page, and the pillar page should link out to all the cluster pages. Be sure to use optimised anchor text terms for your hyperlinks.

### Maintenance of Your Content Cluster

Once established content clusters can be added to regularly, as well as tracking and optimising assets within the cluster. A common question is how many cluster pages should be in a content cluster?

Answer: as many as you can think of. The trick is to continually identify content gaps within your cluster by means of research and to fill these.
