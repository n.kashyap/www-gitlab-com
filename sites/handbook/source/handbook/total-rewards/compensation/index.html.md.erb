---
layout: handbook-page-toc
title: Compensation
description: Find answers to your questions about GitLab's compensation framework.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Related Compensation Pages

* [Compensation Calculator Formula](/handbook/total-rewards/compensation/compensation-calculator)
* [Use the Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator)
* [Compensation Review Cycle](/handbook/total-rewards/compensation/compensation-review-cycle)

## Introduction
On this page, we're detailing why we have the compensation framework we have now.

## GitLab's Compensation Principles

1. We're an open organization, and we want to be as transparent as possible about our compensation principles. Our compensation model is open to data driven iterations.
1. We are [paying local rates](#paying-local-rates) based on [cost of market (also referred to as cost of labor)](https://www.erieri.com/blog/post/cost-of-labor-vs-cost-of-living). There is _no_ cost of living input in our compensation philosophy.
1. Compensation aims to be at a [competitive rate](#competitive-rate) for your job family, your location, your level, your experience, and your contract type.
1. We use a [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator) to ensure transparent and consistent compensation.
1. We do not disclose individual compensation since compensation is [not public](/handbook/communication/#not-public).
1. We adjust our calculator based on survey data, feedback from applicants and team members, and candidate data. Please email total-rewards@ domain if you want to contribute.
1. We offer [stock options](/handbook/stock-options/) for most positions.
1. We base compensation on current position and performance at GitLab – not on what we paid you last month – and, generally, we don't reduce compensation.
1. Increases within the market pay bands will be based on performance.
  * At hire, we base our compensation offer on the position and experience in the market.
  * For promotions, increases are based on ensuring alignment to the new role’s market range.
  * At Annual Compensation review, increases will ensure alignment to the market (by being within the entire range of the calculator), and resources will be allocated to top performing team members within the organization.
1. GitLab will continue to monitor [pay equality](/handbook/people-group/people-success-performance-indicators/#sts=Pay%20Equality) to ensure underrepresented groups are paid at the same rate as the company.
1. We want to follow the processes on this page for everyone, please email total-rewards@domain when we don't. If you have a specific question around your compensation or one of your direct reports' please schedule a call with total-rewards@ domain to review.
1. We will update this page and the processes throughout the year.
1. We'll adjust your pay as soon as your job-family or level factor changes.
1. If we change our [SF benchmark](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark) for a job family without changing the requirements, we change the compensation both for existing team members and new hires. If the SF benchmark is changed together with the requirements this review might happen at the time of the change or in our yearly cycle.
1. We offer [bonuses and incentives](/handbook/incentives), but we don't offer ping pong tables or free lunches. We think it is more important to offer people flexibility and freedom. See the [Top 10 reasons to work for GitLab on our culture page](/company/culture/#advantages/).
1. We hire across the globe, but we're not location agnostic. Your timezone, the location factor in your region, and the vicinity to users, customers, and partners can all be factors. For example, we may favor one applicant over another because they live in a region with a lower location factor or because we need someone in that timezone.
1. All things being equal, factors such as whether a candidate is in an underrepresented group or a lower-cost market vs higher-cost market can help us come to hiring decisions. In this case, both factors are equally weighted and business needs will inform the final hiring decision.
1. People on quota (account executives, account managers, and sales leadership) have variable compensation that is about 50% of their On Target Earnings (OTE). Individual contributors in the sales organization have variable compensation that is purely based on commission.
1. Compensation decisions around level and experience levels and for functions not in the calculator are taken by the compensation group<a name="compensation-group"></a>. This group consists of the CFO, CEO, and Chief People Officer. All requests will first be routed to the CFO and the CPO. If needed, the request can be escalated to the CEO. When there is no time to coordinate with the group, the CEO can make a decision and inform the group. When the CEO is away (e.g. vacation), the two other members of the group can make a decision and inform the group. Whatever the decision is, the earnings committee should be cc-ed (or bcc-ed) on the final email so that the committee members can know that the loop was closed. This group is different from the compensation committee at the [board level](/handbook/board-meetings/#board-and-committee-composition).


## Competitive Rate

**We want our compensation to be at a level where we can recruit and retain people who meet our requirements.**
Our requirements for most of our [job-families](/handbook/hiring/job-families/) are at **or above** the average in the market.
Therefore, we can expect to be at or above the 50th percentile of the survey data gathered from providers like Comptryx and Radford.
Please do not use the term market rate since this can mean either competitive rate or survey data.
Also see our [SF benchmark](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark).

When discussing a competitive rate for a single person at GitLab, please refer to this as their lottery factor. For example, if this person won the lottery and left the company what impact would that have. Other common industry terms are walk away factor or bus factor, but those both hold a negative connotation.

## Paying Local Rates

### Why we pay local rates

Competitive rates for roles vary depending on regions and countries.
We pay a [competitive rate](#competitive-rate) instead of paying the same wage for the same role in different regions.
Paying the same wage in different regions would lead to:

1. If we start paying everyone the highest wage our compensation costs would increase greatly, we can hire fewer people, and we would get less results.
1. A concentration of team members in low-wage regions, since it is a better deal for them, while we want a geographically diverse team.
1. Team members in high-wage regions having much less discretionary income than ones in low-wage countries with the same role.
1. Team members in low-wage regions being in [golden handcuffs](https://en.wikipedia.org/wiki/Golden_handcuffs) and sticking around because of the compensation even when they are unhappy, we believe that it is healthy for the company when unhappy people leave.
1. If we start paying everyone the lowest wage we would not be able to attract and retain people in high-wage regions, we want the largest pool to recruit from as practical.

For more context please also see our [blog post about paying local rates](https://about.gitlab.com/blog/2019/02/28/why-we-pay-local-rates/).

Please see a Twitter thread from compensation expert Heather Doshay where she discusses [paying local rates](https://twitter.com/heatherdoshay/status/1228743322350473217?s=12) and how that impacts both companies and team members.

### Hire the best candidate

We hire the best candidate for each role regardless of location, cost, or other factors.
During the sourcing we do optimize what potential candidates we approach in order to bring more diversity (both geographically and people from underrepresented backgrounds) to our team.



### Blog Post

We also wrote a [blog post about paying local rates](/blog/2019/02/28/why-we-pay-local-rates/).

## Market-Based Approach

We have a market-based approach to compensation because:
1. It allows for us to adopt to market changes, thus remaining competitive for local talent
1. Role, location, and level benchmarks keep compensation bands consistent. Market-based benchmarks also help with keeping it fair and consistent rather than expecting our team members to negotiate their pay.
1. Prevents wage compression which is when new employees negotiate and get higher wages than those being paid to current team members.
1. As long as budget permits, we do what’s right for the market even if it means paying a team member higher than their “expected” pay.

## Compensation for Interim Roles

Effective Q2 of FY 2021, we have established a one time bonus payment process for team members that are asked to step into an interim management role. Team Members acting in an interim management role should review [the expectations of an individual in the management group](/company/team/structure/#management-group).

### Criteria for Eligibility

* For an interim role to be considered, the need for coverage would need to be longer than a 30 day time period.
* The interim role has to be at a higher level than the current role of the team member.
* In case the interim role is within another job family, team members will also be eligible for the interim compensation on a lateral level.

### Calculation of Interim Bonus

The formula for the bonus recognizes the length of time that the team member is playing the interim role. Payment of the one time bonus would occur at the completion of the interim role. The bonus would be calculated using the following formula:

The greater value of the standard discretionary bonus amount ($1,000 at the current exchange rate) OR the following calculation:

For team members on a base salary compensation plan, your bonus will be an additional 10% of your salary for the duration of the interim role period.  The calculation is as follows:
* `(Annual Base Salary in Local Currency/365) x .10 (10%) x # of Calendar Days in the Interim Role`

For team members on an OTE (On Target Earnings) compensation plan, if you assume the quota of the role that you are covering for, you will be paid 10% of your OTE for the interim role period. The calculation is as follows:
* `(Annual On Target Earnings in Local Currency/365) x .10 (10%) x # of Calendar Days in the Interim Role`
* Commissions payments will continue for the duration of the interim role. Once the interim role is complete, your quota will revert to the quota of your previous role and a new compensation plan will be issued memorializing that quota and commission rate.

If your compensation changes during the interim period (for example, relocation, country conversion, etc.), we will calculate the interim bonus based on the pay rate of each calendar day.

The process for submitting an interim bonus is as follows:
* Leader works with their aligned People Business Partner to confirm the start and end dates of the interim role.
* The People Business Partner works with the Compensation group to confirm the bonus amount.
* Once confirmed, the leader would [submit the bonus in BambooHR](/handbook/incentives/#process-for-recommending-a-team-member-for-a-bonus-in-bamboohr).

The team member must be an active team member of GitLab at the end of the interim role period to be eligible to recive a bonus payment. If a team member leaves GitLab during the interim role period, they will not be eligible for a prorated payment.

### Examples of the Interim Bonus Calculations below

* Senior Engineer has a base salary of $125,000. She has taken on the interim role of Engineering Mgr for 3 months (Jan-March) which is a total of 90 days. The bonus for this interim role would be `($125,000/365) x .10 x 90 = $3,082.19`
* Finance Business Partner has a base salary of $100,000. He has taken on an interim role covering multiple teams while his coworker is on leave for 4.5 weeks which is a total of 31 days. The bonus for this interim role would be `($100,000/365) x .10 x 31 = $849.32` so we would round up for this bonus and process as a discretionary award.


## Exchange Rates

<%= partial "includes/people-operations/currency_exchange_listing" %>

### Paid in your local currency

The compensation calculator is updated in January and July with the most recent exchange rate. Due to the current economic climate, we will continue to use January 1st for currency conversion until further notice. Adjustments to existing team members' compensation who are paid in local currency are made as part of the applicable [compensation review cycle](/handbook/total-rewards/compensation/compensation-review-cycle/). This is because since you are paid in local currency, your local purchasing power does not change.

### Not paid in your local currency

There are a number of reasons why team members may not be paid in local currency. For example, GitLab's bank might not support the currency or there may be a strong economic reason for the team member to choose a different currency. The currencies that GitLab can pay in are published on the [contracts](/handbook/contracts#available-currencies) page.

1. The Total Rewards Analyst will analyze the difference in exchange rates from January to July (or vice versa) and the effect this has on local purchasing power using the following process:
  * The exchange rate between the team member's local currency and chosen currency will be collected for both the current and previous conversion effective date.
  * The percent change is calculated between the two rates: `(New Rate - Previous Rate)/Previous Rate`.
1. Criteria for review, the team member:
  * Is not paid in local currency.
  * Has been at GitLab for greater than six months.
  * Has not opted out.
1. Only changes greater than +/- 5% will be processed in each review.
  * Changes must stay within band of the compensation calculator.
1. Total Rewards Analysts will confirm by [letter of adjustment](/handbook/contracts/#letter-of-adjustment) any _increases or decreases_ to compensation as a result of this review.
1. Any significant increases or decreases will be reviewed on a case-by-case basis before any changes are made.
1. Team members may also [opt out](https://docs.google.com/document/d/1GMxFFMKL8ssEi8gPJzpc1xizSJHxrR2n0R5HSZn-G-g/edit?usp=sharing) of the reviews.
  * If an opt out agreement is signed, the team members will be excluded from any future Exchange Rate reviews. GitLab retains the right to revisit the opt-out status in the future.
1. Additionally, if a team member is impacted outside of this review period they should reach out to total-rewards@domain.

Example:  A team member's local currency is the Russian Ruble (RUB) and they are paid in US Dollars (USD). They have not previously opted out and have been employed for greater than 6 months. The exchange rate effective 2020-01-01 is 0.016. If for 2020-07-01 the exchange rate increases to 0.017, then this would result in a percent increase of 6.25%. The team member would have the option to either accept this increase to their salary or opt out.

#### Special Notice for 2020-07-01 Local Purchasing Power Review

Due to the current economic climate, we will not be applying salary fluctuations based on the 2020-07-01 local purchasing power review for those not paid in their local currency. We will continue to monitor the economic situation and review the impact to the team before reinstating this process.

## Hourly Employees

GitLab is committed to following all applicable wage and working hours laws and regulations. To help ensure that all work performed for GitLab is compensated correctly, team members compensated on the basis of hours worked must [report and record time](/handbook/finance/#timesheets-for-hourly-employees)accurately.

## Sales Compensation

Sales and Sales Development roles at GitLab that are subject to quota and paid commission based on the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/) are not located in the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/). This includes roles in the following departments:
* Channel
* Commercial Sales
* Enterprise Sales
* Sales Development

These roles are still [benchmarked using market data](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark), but instead of using a [location factor](/handbook/total-rewards/compensation/compensation-calculator/#location-factor), a sales geographic differential is applied based on the team member's country/region alignment according to the following table:

| Country         | Sales Regional Differential |
|-----------------|-----------------------------|
| APAC            | 0.70                        |
| Australia       | 0.85                        |
| Japan           | 0.85                        |
| Switzerland     | 0.95                        |
| Eastern Europe  | 0.60                        |
| Northern Europe | 0.80                        |
| Southern Europe | 0.60                        |
| Western Europe  | 0.85                        |
| United Kingdom  | 0.85                        |
| Africa          | 0.60                        |
| LATAM           | 0.60                        |
| Canada          | 0.80                        |
| United States   | 1.00                        |

## Variable Pay Frequency

The Variable Pay Frequency is a field held in BambooHR and applicable to roles that receive a commission or bonus. Variable Pay Frequency is assigned according to the following chart:

| Department/Role Description | Variable Pay Frequency Type |
|-----------------------------|-----------------------------|
| VP (non-sales), Executives  | [Company Executive Bonus Program](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan) |
| VP (Channel, Field Operations, Customer Success) | [Sales Executive Bonus Program](/handbook/total-rewards/compensation/#sales-director-and-above-bonus-plan) |
| VP (Commercial Sales, Enterprise Sales) | Monthly Bonus |
| Director (non-sales) | [Company Director Bonus Program](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan) |
| Director (Field Operations) | [Sales Director Bonus Program](/handbook/total-rewards/compensation/#sales-director-and-above-bonus-plan) |
| Director (Channel, Customer Success, Enterprise Sales) | Monthly Bonus |
| Director (Consulting Delivery) | Quarterly Bonus |
| Channel, Commercial Sales, Customer Success, Enterprise Sales, Sales Development (IC) | Monthly Bonus |
| Consulting Delivery, Education Delivery, Practice Management, Sales Development (Manager) | Quarterly Bonus |


## Director Compensation

In addition to base compensation, Directors who are not already enrolled in the Sales Compensation Plan or other performance incentive plan are eligible for a 15% bonus of current base salary (increased from 10% beginning in FY 2021). Director Compensation is determined as part of the [GitLab Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/).

## Executive Compensation

Executive Compensation (VP and above) is derived outside of the GitLab Compensation Calculator using the following process:

1. The Total Rewards team collects market data to review against current compensation to ensure alignement in base and variable.
  * Market Data from the GitLab peer group: In FY21 the peer group data used came from Radford, in FY22 the peer group data will be from Compensia.
  * AdvancedHR Market Data for a Private Pre-IPO Company with 5+ rounds of funding.
1. The Total Rewards team will advise on a market increase or performance factor as determined in the [performance enablement review](/handbook/people-group/learning-and-development/career-development/#performance-enablement-review), whichever is appropriate.
1. The recommendation will be reviewed by the Compensation Group ensuring alignment to budget with the CFO.
1. If approved by the Compensation Group, the Total Rewards team will escalate the CEO's direct reports to the Compensation Committee for approval.
1. The Total Rewards team will then notify the CEO or direct manager of the increase who will then communicate it to the individual.

For FY22, GitLab will review how we can incorporate performance into the Executive Compensation increase process.

As each Executive has an individual market assessment upon hire, we expect compensation to be aligned to market at each compensation review. If there are large changes in the market for a specific role, those will be addressed.

## Company Director and above Bonus Plan

Targets:
50% of the bonus is based on performance against [IACV](/handbook/sales/#incremental-annual-contract-value-iacv).
50% of the bonus based on performance against [TCV](handbook/sales/#total-contract-value-tcv) less Operating Expenses (including COGS).

All targets are based on Company performance against Plan targets as approved by the Board of Directors.

**Thresholds:**
* Each target is subject to a threshold. That is, if Company performance is less than the threshold no bonus is earned. Above the threshold the bonus is earned based on the percentage achievement above or below the threshold.  For example at 90% of Plan the bonus is achieved at 90%; at 110% of Plan the bonus is achieved at 110%.
* The IACV target carries an 80% threshold and the TCV less OpEx carries a 90% threshold.
* The thresholds operate independently such that if one component is not achieved bonus may still be earned on the other component.
* Each target is subject to a cap of 200%.

**Payout:**
* The bonus will be paid out semi-annually.
* The first semi-annual payment will be at the lower of i) 80% of Company performance against targets or ii) 80% of plan performance. Any full plan year adjustments will be made as part of the final bonus payout.
* The final bonus payout will be based on actual performance against plan less amount paid at the semi-annual payout.
* If the first semi-annual bonus is achieved but Company performance falls below the annual threshold the second half bonus will not be achieved but the first half bonus will not be rescinded or otherwise payable back to the Company.

**Communication**
* Prior to the bonus payment, the CFO will confirm the achievement % of the bonus plan in the #director-and-above slack channel.

**Timing:**
* Payments expected to occur 60 days after the end of the fiscal period but are dependent on the accounting close process and board approvals.


**Approvals:**
* The bonus plan is approved by the [Compensation Committee](/handbook/board-meetings/#compensation-committee) of the Board of Directors.
* Performance against Plan targets is reviewed and approved by the [Audit Committee](/handbook/board-meetings/#audit-committee) of the Board of Directors.
* Bonus payouts are recommended by the CFO and approved by the Compensation Committee of the Board of Directors.

**Proration and Eligibility:**
* The Director bonus will be pro rated for those team members who start after Feb 1st, or are promoted after August 1st.
* In addition, the team member must be actively employed by GitLab at the time of payout.

Bonus examples can be found in the following [google doc](https://docs.google.com/document/d/1G_CQQ8GBtqFvwUlOH8WpxddqPMCH2HdHbl1I_fJ-jB0/edit?usp=sharing).

GitLab reserves the right to change or discontinue the bonus program at any time. All bonus payouts are discretionary and require the achieving of specific company results that will be communicated each year.

### Sales Director and above Bonus Plan
There are Directors in the Sales organization that are tied to the Company Director and Above Bonus Plan for a portion of their OTI (on target incentive). These team members are notified of this compensation element in their compensation plan. See the breakdown in percentages below:

Targets:
92% of the bonus is tied to a quota.
8% of the bonus is aligned to the [Company Director and above Bonnus Plan](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan).

**Thresholds:**
* The part of the bonus aligned to the [Company Director and above Bonnus Plan](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan) is subject to a threshold. For more information, please see the Thresholds section for the [Company Director and above Bonnus Plan](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan).

Payout, Communication, Timing, Approvals, Proration and Eligibility, and Bonus Payout Approvals will be the same as for the [Company Director and Above Bonus Plan](/handbook/total-rewards/compensation/#company-director-and-above-bonus-plan).

### Bonus Payout Approvals

The Compensation Committee approves all bonus payouts for direct reports to the CEO at the end of each Fiscal Year. The Compensation Group internally at GitLab approves all other bonus payouts.

1. The Finance team will generate a calculation of the total bonus payout for the Fiscal Year within 30 days of the last day of the fiscal year.
1. The Total Rewards Team will audit the calculation.
1. Once approved by the Total Rewards Team, the CFO and CPO will review and approve all final numbers.
1. Once approved by the CPO and CFO, the Total Rewards Team will generate a google sheet summarizing the data for the direct reports of the CEO. This sheet should include the following headers: Employee #, Name, Reporting to, Division, Department, Title, Hire Date, Total Eligible, Total Payout. Any additional notes should be added to the bottom of the sheet.
1. The Total Rewards Team will communicate the final numbers via email to the Compensation Committee for approvals and ccing the CFO, CPO, CEO, and CLO in the communication.
1. Once approved by the Compensation Committee, the Total Rewards team will notify payroll that the bonuses are ready for processing.
1. Once the compensation group internally at GitLab approves all indirect reports to the CEO, the Total Rewards Team will notify payroll that the bonuses are ready for processing.

## Compensation Data Analysis and Modeling Sheet

This is an internal process for the Total Rewards team which details how to refresh the Compensation Data Analysis and Modeling sheet which is used for processes such as calculating [percent over compensation band](https://about.gitlab.com/handbook/people-group/people-group-metrics/#percent-over-compensation-band).

To update:
1. Navigate to the sheet "Comp Data Analysis and Modeling - BambooHR Report".
1. Download the "Comp Data Analysis & Modeling" report from BambooHR as a CSV.
1. Duplicate the "Template" tab in the sheet and rename it based on the date the BambooHR report was pulled using format: yyyy-mm-dd.
1. Copy and paste the BambooHR report starting from cell A2.
1. Delete any test accounts, denoted "Test" in their name, by deleting the entire row.

## Learning GitLab's Compensation Framework

As part of our Q1 OKR, we will be working on ensuring there are materials for a compensation certification. The following are the initial questions to generate the certification.

### The Why Questions

* [Why do we have the compensation framework we have now?](/handbook/total-rewards/compensation/#gitlabs-compensation-principles)
* [Why does GitLab aim to pay a competitive rate?](/handbook/total-rewards/compensation/#competitive-rate)
* [Why does GitLab pay local rates?](/handbook/total-rewards/compensation/#paying-local-rates)
* [Why does GitLab have a market-based approach to compensation instead of performance-based?](/handbook/total-rewards/compensation/#market-based-approach)
* [Why does GitLab have a Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator)

### The What Questions

* [What is the foundation of GitLab's compensation framework?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator)
* [What is the formula of the Compensation Calculator or otherwise known as the Calculator "Inputs" or "Factors"?](/handbook/total-rewards/compensation/compensation-calculator/#the-compensation-calculator-formula)
* [What does the SF Benchmark mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#sf-benchmark)
* [What does the Location Factor mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#location-factor)
* [What does the Compa Ratio mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#compa-ratio)
* [What does the Contract Factor mean on the Compensation Calculator?](/handbook/total-rewards/compensation/compensation-calculator/#contract-factor)
* [What does the Exchange Rate mean on the Compensation Calculator?](/handbook/total-rewards/compensation/#exchange-rates)

### The How Questions

* [How do we continue to make sure that our team members are compensated according to their skill level and receive equitable pay?](/handbook/total-rewards/compensation/compensation-review-cycle/#compensation-review-cycle)
* [How do we make sure that the different Compensation Calculator inputs remain relevant and competitive to market?](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review)
* [How does the Total Rewards team carry out the Annual Compensation Review?](/handbook/total-rewards/compensation/compensation-review-cycle/#annual-compensation-review-timeline)
* [How do we make sure that new team members joining GitLab between November and January will receive a compensation review every 12 months?](/handbook/total-rewards/compensation/compensation-review-cycle/#catch-up-compensation-review)

## Knowledge Assessment

You can test your knowledge on our compensation by taking the [GitLab Compensation Knowledge Assessment](https://docs.google.com/forms/d/e/1FAIpQLSe7cf8j9EMk3raWY7nHwSSVSdO0eE7bSHZ8TxSbm-L7hgQvuw/viewform) quiz.

If you have qustions about compensation or the content in the Knowledge Assessment, please reach out to the [Total Rewards](/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group) team. If the quiz is not working or you have not recieved your certificate after passing the Knowledge Assessment, please reach out to the [Learning & Development](/handbook/people-group/learning-and-development/#how-to-communicate-with-us) team.
