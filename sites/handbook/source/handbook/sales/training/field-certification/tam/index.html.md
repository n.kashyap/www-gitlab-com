---
layout: handbook-page-toc
title: "Field Certification: Technical Account Managers"
---

## Field Certification Program for Technical Account Managers 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview 
To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a certification program for Technical Account Managers that includes functional, soft skills, and technical training for field team members.  

Certification badges will align to the customer journey and critical “Moments That Matter” (MTMs) as well as the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute each MTM.

## Technical Account Managers Curriculum 
The below slide shows the holistic learner journey for TAMs and provides context for the information included throughout the learning program. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQvjB6E9JlplzwqBHVv2fFGAEGZwqjg4AZQO-p_DqjX7znjZGOC_q2-d2xCbwr2LbfXCmyOvVxcirYb/embed?start=false&loop=false&delayms=3000&slide=id.g94bb3b04a3_0_492" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Onboarding Customers for Success Learning Path 
This is the first learning path in development for TAMs which will consist of five courses. The learning objectives for each course are outlined below.

**Course 1: SALSA-TAM Meeting**
* Identify roles & responsibilities in the SALSA-TAM Meeting
* Review SALSA-TAM Meeting best practices
* Leverage tools during call 

**Course 2: Transition to the Account** 
* Differentiate between when a TAM should get involved in Commercial and Enterprise accounts 
* Review the customer use case with SAL and SA
* Analyze the customer’s background
* Identify gaps in understanding for planning phase 

**Course 3: Kickoff the Customer Engagement**
* Define current and desired state for customer 
* Customize and send TAM Welcome email 
* Conduct pre-kickoff activities 
* Conduct the kick-off 
* Manage customer onboarding in Gainsight 
* Schedule cadence call to discuss future growth 

**Course 4: Build the Success Plan**
* Review the customer’s motivation for purchasing GitLab through reviews with the sales team
* Identify the customer’s business objectives 
* Document the success plan in Gainsight  
* Track value of the ROI success plan 

**Course 5: Obtain Customer Feedback** 
* Send customer satisfaction survey and swag email 


# Recognition
Upon completing each course, the associate will receive a badge. 

# Feedback 
We want to hear from you! You can follow along with course development by checking out the issues on the [Field Cert Team Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426?&label_name[]=field%20certification). 

