---
layout: handbook-page-toc
title: "Business Systems: Enterprise Applications, Integrations, and Flow"
description: "Business Systems Lead to Fulfillment Documentation"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Lead To Cash Process Components

<div class="limit-width">
<div class="grid-container">
<div class="grid-item-large" style="grid-row-start: 1;grid-row-end: 2; grid-column-start: 1;grid-column-end: 2;background-color: #c4c4c4"><b>Business Process</b></div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #c4c4c4"><b>Supporting System</b></div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #c4c4c4"><b>Supporting Team</b></div>
<div class="grid-item-large" style="grid-row-start: 2;grid-row-end: 4; grid-column-start: 1;grid-column-end: 2;background-color: #fdbc60"><b>Market</b></div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #fca121">Demand Gen Effort (Marketo)</div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #fc9403">Marketing Ops</div>
<div class="grid-item-small" style="grid-column-start: 2;grid-column-end: 4;background-color: #fca121">Trial (Home Grown Application - Ruby on Rails)</div>
<div class="grid-item-small" style="grid-column-start: 4;grid-column-end: 5;background-color: #fc9403">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 4;grid-row-end: 5;"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="grid-row-start: 5;grid-row-end: 8;grid-column-start: 1;grid-column-end: 2;background-color: #f2b4a9"><b>Sell</b></div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 2;grid-column-end: 3;">Digital Purchase Path</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Order Form Purchase Path</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Sales Ops/ Deal Desk</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 2;grid-column-end: 3;">CustomerDot</div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Sales Cycle (SFDC)</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Sales Systems</div>
<div class="grid-item-empty" style="grid-column-start: 2;grid-column-end: 3;"></div>
<div class="grid-item-small" style="background-color: #e67664; grid-column-start: 3;grid-column-end: 4;">Contract (Salesforce, Zuora CPQ, Sertifi)</div>
<div class="grid-item-small" style="background-color: #e05842; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 8;grid-row-end: 9"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #d1d1f0; grid-row-start: 9;grid-row-end: 12; grid-column-start: 1;grid-column-end: 2;"><b>Bill</b></div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Zuora Subscription Lifecycle Management</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Billing</div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Zuora Billing</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Finance</div>
<div class="grid-item-small" style="background-color: #a6a6de; grid-column-start: 2;grid-column-end: 4;">Rev Rec (Netsuite)</div>
<div class="grid-item-small" style="background-color: #7c7ccc; grid-column-start: 4;grid-column-end: 5;">Enterprise Apps</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 12;grid-row-end: 13"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #73afea; grid-row-start: 13;grid-row-end: 14; grid-column-start: 1;grid-column-end: 2;"><b>Fulfill</b></div>
<div class="grid-item-small" style="background-color: #2e87e0; grid-column-start: 2;grid-column-end: 4;">License (Home Grown Application - Ruby on Rails)</div>
<div class="grid-item-small" style="background-color: #1f78d1; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-empty" style="grid-column-start: 1;grid-column-end: 5;grid-row-start: 14;grid-row-end: 15"><i class="fas fa-arrow-down"></i></div>
<div class="grid-item-large" style="background-color: #75d09b; grid-row-start: 15;grid-row-end: 18; grid-column-start: 1;grid-column-end: 2;"><b>Maintain</b></div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 4;">Support (Zendesk)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">Support Team</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 4;">Monitor (Home Grown Application "Version & SeatLink" - Ruby On Rails)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">Fulfillment Engineers</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 2;grid-column-end: 3;">Digital Renewal</div>
<div class="grid-item-small" style="background-color: #37b96d; grid-column-start: 3;grid-column-end: 4;">Order Form Renewal (SFDC, Z-CPQ, Sertifi)</div>
<div class="grid-item-small" style="background-color: #168f48; grid-column-start: 4;grid-column-end: 5;">See Sell</div>
</div>
</div>

## Contact

### People

- **[Business Systems Analyst - Jamie Carey](/job-families/finance/business-system-analyst/)**
- **[Integrations Engineer - Daniel Parker](/job-families/finance/integrations-engineer)**
- **[Product Manager, Fulfillment - Amanda Rueda](/job-families/product/product-manager/)**

### Open an issue

<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">BSA</a>
  <a href="https://gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/issue-tracker/-/issues/new" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Enterprise Applications Integrations</a>
  <a href="https://gitlab.com/gitlab-com/support/license-and-renewals/-/issues" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">L&R team tracker</a>
  <a href="https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Fulfillment</a>
  <a href="https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Systems</a>
  <a href="https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Sales Systems</a>
  <a href="https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=" class="btn btn-purple-inv" style="width:20%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Marketing Ops</a>
</div>

### Slack channels

- [`#bt-finance-operations`](https://gitlab.slack.com/archives/CSTMYD5E1)
- [`#business-operations`](https://gitlab.slack.com/archives/CCPG8P3K4)
- [`#btg-integrations`](https://gitlab.slack.com/archives/C015U7R5XJ8)
- [`#g_fulfillment`](https://gitlab.slack.com/archives/CMJ8JR0RH)
- [`#s_growth_engineering`](https://gitlab.slack.com/archives/CNLHTE457)
- [`#s_growth_pm`](https://gitlab.slack.com/archives/CRM7PCFSR)
- [`#business-fulfillment-sync`](https://gitlab.slack.com/archives/CR0FUJRRV)
- [`gtm-operations`](https://gitlab.slack.com/archives/GEMFSJ94L)

### Engagement meetings

- [Business Fulfillment Sync wiki](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/business-fulfillment-sync/-/wikis/home)
- [SFDC<>Zuora Tech Sync wiki](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/sfdc-zuora-sync/-/wikis/home)

## Enterprise Application Ecosystem

tier one applications of lead to fulfillment

### CustomersDot

- This application is where the customer logs in to manage their account details for their subscription
- The CustomersDot also sends and receives data to and from other applications like: Zuora, Salesforce, GitLab.com, and the license application.
- GitLab created and manages the CustomersDot.
- Additionally, there are two other points of sale online: via the [AWS marketplace](https://aws.amazon.com/marketplace/seller-profile?id=9657c703-ca56-4b54-b029-9ded0fadd970) and [GOV.UK Digital Marketplace](https://www.digitalmarketplace.service.gov.uk/g-cloud/services/526446644571793).

### Salesforce

### Zuora

### EntApp Ecosystem issues and boards

<details>
<summary markdown='span'>
  issue lists
</summary>

- [all issues with EntApp Ecosystem label](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=EntApp%20Ecosystem)

</details>

<details>
<summary markdown='span'>
  issue boards
</summary>

- [gitlab-org Growth team board by milestone](https://gitlab.com/groups/gitlab-org/-/boards/1553395?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=EntApp%20Ecosystem)
- [EntApp Integration Work label in gitlab-org](https://gitlab.com/groups/gitlab-org/-/boards/1862236?&label_name%5B%5D=EntApp%20Integration%20Work)
- [EntApp Integration Work label in gitlab-com](https://gitlab.com/groups/gitlab-com/-/boards/1862240?&label_name%5B%5D=EntApp%20Integration%20Work)
- [gitlab-services](https://gitlab.com/groups/gitlab-services/-/boards/1582585?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=EntApp%20Ecosystem)
- [gitlab-data](https://gitlab.com/gitlab-data/analytics/-/boards/1723125?&label_name%5B%5D=EntApp%20Ecosystem)
- [Sales Systems by EntApp Ecosystem label](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/-/boards/1877940?&label_name%5B%5D=EntApp%20Ecosystem)
- [Potential work for EntApp Integration team - by Security label](https://gitlab.com/gitlab-org/customers-gitlab-com/-/boards/1735210?label_name%5B%5D=security)

</details>

<details>
<summary markdown='span'>
  related epics
</summary>

- [Improve CustomersDot to GitLab.com provisioning sync](https://gitlab.com/groups/gitlab-org/-/epics/3379)
- [GitLab.com <> CustomersDot, License, Zuora Integration](https://gitlab.com/groups/gitlab-org/-/epics/3602)

</details>

##### team boards

<details>
<summary markdown='span'>
  not filtered by EntApp Ecosystem label
</summary>

- [Sales Systems](https://gitlab.com/groups/gitlab-com/-/boards/1117318?label_name%5B%5D=SalesSystems)
- [Growth Team](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth)
- [Growth Team: System Integrity's issues](https://gitlab.com/groups/gitlab-org/-/boards/1674684?label_name%5B%5D=Business%20Integration)
- [BTG Integrations: Finance projects](https://gitlab.com/groups/gitlab-com/-/boards/1780217?label_name%5B%5D=BTG%20Integrations)

</details>

##### relevant repos and projects

<details>
<summary markdown='span'>
  Sales Systems, Growth Engineering, Support Ops, Ent App Integrations
</summary>

- [Salesforce](https://gitlab.com/gitlab-com/sales-team/field-operations/salesforce-src)
- [Zendesk](https://gitlab.com/gitlab-com/support/support-ops)
- [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com)
- [Version app](https://gitlab.com/gitlab-services/version-gitlab-com)
- [License app](https://gitlab.com/gitlab-com/license-gitlab-com)
- [BTG EntApp Integration project](https://gitlab.com/gitlab-com/business-ops/integrations-entapp-ecosystem)

</details>

##### labels

- `EntApp Ecosystem`: in groups gitlab-com, gitlab-org; gitlab-services; gitlab-data; used for anything related to the enterprise application ecosystem integration and data flow
- `EntApp Integration Work`: in groups gitlab-com, gitlab-org; used for flagging work that is specific to the enterprise application integrations
- `System Integrity`: in group gitlab-org; used specifically to flag work on the integration between the CustomersDot and Zuora or Salesforce
- `System Integrity`: in group gitlab-org; used by the Fulfillment System Integrity team to flag work to improve the system reliability and integrations
- `Affects Salesforce`: in group gitlab-org; used by the Growth team to flag any work that could affect data in Salesforce and indicates that the Sales Systems team should review the code
- `EntApp Gap`: in groups gitlab-com, gitlab-org; used to flag gaps in automation of integrations in the Enterprise Application Ecosystem

## Resources

##### high level

- [WIP: view only Lucidchart diagram lead to fullfillment system flow](https://app.lucidchart.com/documents/view/fe61ff48-c0e3-4f40-b2de-4023d48101d9/0_0)
- [video of custom setup](https://drive.google.com/drive/folders/1kfCEQM6XYGWYxq3Ke4TNvtmDR-46erVD)
- [Security's Compliance Diagram](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/blob/master/PCI/customers.gitlab.com_data_flow_diagram_-_New_Business.pdf)
- [Growth Team's Web store and CustomersDot Flow Diagram](https://app.mural.co/t/gitlab2474/m/gitlab2474/1569500330861/8f9fd73826c42ad809d51be886db27494da91353)
- [Trade Compliance](/handbook/business-ops/trade-compliance/)
- [Sales flow](https://drive.google.com/file/d/1nkJrsXewy1G9llV9-8k2EhineU2hyoDJ/view?usp=sharing)
- [entry points, integration users google sheet](https://docs.google.com/spreadsheets/d/1j3xE6pQLfsKMri14LDcrnxbWbTwqz4Tpv9kI8UIHYCE/edit#gid=0)
- [PCI In-Scope Systems diagram](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/blob/master/PCI/PCI_In_Scope_Systems.md)
- [entry points and conversion by marketing](https://www.figma.com/file/JMFCXAftW30wjul6TTIPFa/lead%2Fsign-up-flow?node-id=0%3A1)
- [customer facing documentation on managing subscriptions](https://docs.gitlab.com/ee/subscriptions/)
- [UX flows for trials](https://app.mural.co/t/gitlab2474/m/gitlab2474/1580984258623/a815e52decef6141307b634da65fbcd5242a48e8)

##### Self-managed

- [renewal UX flow](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/75)
- [Usage ping on Self-managed](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#request-flow-example-1)
- [Self-managed purchase flow video](https://www.youtube.com/watch?v=y_j1ini0qrA)

##### SaaS

- [SaaS renewal flow diagram](https://whimsical.com/S4r7nuRFgQ5kZKaZisZcoQ)
- [SaaS purchase flow video/customer app](https://www.youtube.com/watch?v=p32PRZEzhg0)
- [trial SaaS video](https://gitlab.zoom.us/recording/play/sGNG2-Ny2wIr4tsWcPHNWaoDDyC-EkSvKJEBeAO54Mz4KhF0pw9Kn66OrBcjJHms?continueMode=true)

##### CI minutes

- [walkthrough video of purchasing CI minutes](https://drive.google.com/drive/folders/1UxEVNgAkL2TwH8NOOmT4a6cFL_vqIZCH)

##### rough drafts

<details>
<summary markdown='span'>
  Notes to be turned into proper documentation
</summary>

- [WIP: Lucidchart diagram lead to fulfillment through systems](https://www.lucidchart.com/documents/edit/fe61ff48-c0e3-4f40-b2de-4023d48101d9/0_0)
- [Slack convo around customer web store purchase system order of operations](https://docs.google.com/document/d/1tujf8SrpoTx-5ToFy33LIRk5VpfW03aQWDDeaUIJGsM/edit)
- [Webstore and CustomersDot Changes - Reseller Deals](https://docs.google.com/document/d/1uDsOPtGhkrEhSDB8vhhX8VHbvsOlOj3RekK5sveVGgs/edit#heading=h.orzte2ssic3m)
- [GitLab.com <> Customers <> Salesforce integration](https://docs.google.com/document/d/1Q3aL3DbfAsYfpG9r5y3KbxZ-4rk38e6FjYKzktnsZQo/edit)

</details>

## Diagrams

##### trial web direct system flow

- tab 1: trial flows for SaaS and Self-managed through Tier 1 applications
- tab 2: includes LeanData

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/46865cd9-a20f-4700-b1f7-a78e5979f9cf" id="Rc-ygnLVD2A7"></iframe></div>
</div>

##### new business system flow

- first tab: direct via Sales
- second two tabs: SaaS and Self-managed web direct
- includes abandoned cart for web direct flows and contact sales 

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/56f1f1b3-68f2-4609-a867-711f1fdaca15" id="tc-yhP~KqHYz"></iframe></div>
</div>

##### renewals system flow

- includes cancellation flow
- four tabs
    - direct
    - web direct SaaS (auto-renew = on vs off)
    - web direct Self-managed (auto-renew = on vs off)
    - future state: automated web direct

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/5845e3b1-d8fc-453c-984f-3c66b1dbea3d" id="4b-y4oHm3itk"></iframe></div>
</div>

##### customer flow

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://www.lucidchart.com/documents/embeddedchart/d5593761-efdf-43a0-bfe5-717d787404d7" id="_OtpjB8oRZGk"></iframe></div>
</div>

##### overall lead to fulfillment

<div class="center">
<div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://app.lucidchart.com/documents/embeddedchart/b5a0e4b3-ff40-4685-8d21-3109696c8f64" id="X~9ywPrrncS-"></iframe></div>
</div>

##### Zuora and Salesforce

<img src="/handbook/business-ops/images/zuoraSFDC1.png" class="full-width">

## System Integrations Requirements

- API should send country codes ISO 2 from CustomersDot or GitLab.com forms to SFDC and Marketo
- [Required fields](https://gitlab.com/groups/gitlab-org/-/epics/3627) for submitting new leads to Marketo in order for them to be properly rotated to the sales team.

## Zuora Configurations

- [overall Zuora system configurations](https://docs.google.com/spreadsheets/d/11VnuTlWuMe6XGf6gB0_x16MSQvJryuEY8gQRx66wjJ4/edit?ts=5f0f28d0#gid=152692183)
- [finance systems integrations inventtory](https://docs.google.com/spreadsheets/d/1H2DTuP0lS6J129pProMajhdH_as6i38RV1_FjA59uM8/edit#gid=0)
- [heatmap of finance systems](https://app.lucidchart.com/invitations/accept/df465e7e-de32-4e6f-b93d-0c89044bca5c)

<details>
<summary markdown='span'>
  Zuora Communication Profiles
</summary>

- Communication Profiles:
    - New accounts are set to `Default Profile`
    - `Silent Profile` is used temporarily when the customer should not receive notifications.
        _note: this would prevent the system from making the license available in the CustomersDot._

</details>

<details>
<summary markdown='span'>
  Zuora: relationship between billing batches and auto-renew
</summary>

- Batch1: Web Direct
- Batch2: Sales assisted
- Batch3: Auto-Renewal
- Batch4: Reseller
- Batch5: EDU/OSS

Billing batches support more optimization of billing functions in Zuora.

Auto-renew functionality is distinct from billing batches.
_Auto-renew in the CustomersDot must be set by the customer and functionality in the CustomersDot is distinct from the functionality in Zuora._

- SaaS purchases in the CustomersDot default is `auto-renew=true`
- if a customer cancels their subscription, `auto-renew` toggles to `false`
- in Zuora, subscriptions default to `auto-renew=true`
- for sales assisted deals, the billing team manually sets `auto-renew=false`, but the customer can change it to `auto-renew=true` from the CustomersDot

</details>

<details>
<summary markdown='span'>
  Zuora SKUs
</summary>

- [Product list in Zuora](https://docs.google.com/spreadsheets/d/1CtYcjutbuZ8FrxMN0BdVXSc8nWahjhcgoq2Gmb0R-xc/edit#gid=0)

</details>

<details>
<summary markdown='span'>
  Zuora Entities
</summary>

- All web direct accounts are sent to GitLab, Inc. (US) entity for initial purchase.
    If a deal moves to sales assisted for any reason, we then create an account manually on the correct entity if it differs from US.

The defaults are related to the address delineating the entity based on the location of the customer.
Country specification is unrelated to a close process.
Note that the GitLab entity information will be populated via the following rules.
This table is based on the ISO-2 billing country code of the direct customer or reseller we are delivering invoices to:

| Entity               | Direct/Unauthorized Reseller | Authorized Reseller   |
| -------------------- | ---------------------------- | --------------------- |
| BV (Netherlands)     | NL                           | Not AU, DE, UK, or US |
| GmbH (Germany)       | DE                           | DE                    |
| Ltd (United Kingdom) | UK                           | UK                    |
| Inc (United States)  | Not AU, NL, DE, or US        | US                    |
| Pty Ltd (Australia)  | Not NL, DE, UK, or US        | AU                    |

For example:
A sales assisted customer or unauthorized reseller in the Netherlands (NL) will be billed out of GitLab BV; Germany from GitLab GmbH; United Kingdom from GitLab Ltd; any sales assisted customer outside these countries will be billed from GitLab Inc.
An authorized reseller based on Germany will be billed from GitLab GmbH; United Kingdom from GitLab Ltd; United States from GitLab Inc; any resellers based outside these countries will be billed out of GitLab BV.

</details>

## Notifications

<details>
<summary markdown='span'>
  renewal mailings
</summary>

This flow diagram shows the current renewal email cadence, depending on auto-renew settings.
Note: Email content cannot currently be tailored to SaaS vs. Self-managed.
<img src="/images/growth/renewal_emails.png">

- The main difference between the flows is whether auto-renew is on or not
- The emails come from Zuora, with the exception of the "14 day SaaS auto-renew email", which is sent by the CustomersDot
    - 14 day SaaS auto-renew email is only sent if there is an increase in the customer's seat count which will lead to an increased bill

### 90 day email

<a href="/images/growth/renewal_mailing_90.png"><img src="/images/growth/renewal_mailing_90.png"></a>

### 45 day email

<a href="/images/growth/renewal_mailing_45.png"><img src="/images/growth/renewal_mailing_45.png"></a>

### 21 day email (auto-renew on)

<a href="/images/growth/renewal_mailing_21_3.png"><img src="/images/growth/renewal_mailing_21_3.png"></a>

### 15 day email

<a href="/images/growth/renewal_mailing_15.png"><img src="/images/growth/renewal_mailing_15.png"></a>

### 14 day email (auto-renew on and having an increase)

<a href="/images/growth/renewal_mailing_14.png"><img src="/images/growth/renewal_mailing_14.png"></a>

### 3 day email (auto-renew on)

<a href="/images/growth/renewal_mailing_21_3.png"><img src="/images/growth/renewal_mailing_21_3.png"></a>

### 0 day email

<a href="/images/growth/renewal_mailing_0.png"><img src="/images/growth/renewal_mailing_0.png"></a>

</details>

<details>
<summary markdown='span'>
  customer application emails
</summary>

Emails notifications are sent through Mailgun

- Reseller Notification: this email is delivered from the CustomersDot after the customer accepts the EULA.
    - [HAML/HTML](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/5e50769b1ef64f9002ecf9b59dbba5cc24031733/app/views/customer_mailer/reseller_notification_license_sent.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/5e50769b1ef64f9002ecf9b59dbba5cc24031733/app/views/customer_mailer/reseller_notification_license_sent.text.erb#L3)
- Customer - Accept Terms Request: this email is delivered from the CustomersDot when the EULA is required to be accepted before delivering the license.
    - [HAML/HTML](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/3248ac5978678b6920d7cb755be288e312fda8aa/app/views/customer_mailer/accept_terms_request.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/customers-gitlab-com/blob/3248ac5978678b6920d7cb755be288e312fda8aa/app/views/customer_mailer/accept_terms_request.text.erb#L4)
- Customer - License Email: this email is delivered from the license app after the license is created following the EULA acceptance.
    - [HAML/HTML](https://gitlab.com/gitlab-org/license-gitlab-com/blob/12dffa87b8b1c092b7abebc63a7aaae61528f68f/app/views/customer_mailer/license_email.html.haml#L1)
    - [Text](https://gitlab.com/gitlab-org/license-gitlab-com/blob/f8e7e146b10319640176e1cedf395c5a83c325bd/app/views/customer_mailer/license_email.text.erb#L1)

</details>

## Definitions

<details>
<summary markdown='span'>
  subscriptions
</summary>

`Start Date` (Subscription): the start date of this transaction

- `Subscription Term Start Date`: the start date of the subscription itself
- [`active_user_count`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage_data.rb#L32): is pulling any user who is not blocked or deactivated, used in usage ping
- [`current_active_users_count`](https://gitlab.com/gitlab-org/gitlab/blob/master/ee/app/models/historical_data.rb#L15): the active user count applicable for billing purposes and captured in the daily cron job. This is labeled `billable_users` in usage ping. This value does not count the users which are not applicable to billing:
    - GitLab generated service bots
    - Guest users in Ultimate subscriptions

</details>

<details>
<summary markdown='span'>
  Self-managed EE users
</summary>

- a license holder is someone who pays for a GitLab license and manages the payments for a Self-managed installation.
[EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms), [FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), [EE LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE)
- a Self-managed admin is someone who manages the installation and users of a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), <https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE>
- a Self-managed user is someone who uses a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE), <https://gitlab.com/gitlab-org/gitlab/blob/master/ee/LICENSE>

</details>

<details>
<summary markdown='span'>
  dotcom
</summary>

- a dotcom subscriber is someone who pays for dotcom.
[EULA](https://customers.gitlab.com/admin/eula), [TOU](https://about.gitlab.com/terms)
- a dotcom user are people who can be assigned to seats that a dotcom subscriber is paying for.
[TOU](https://about.gitlab.com/terms)

</details>

<details>
<summary markdown='span'>
  Self-managed FOSS
</summary>

- a Self-managed admin is someone who manages the installation and users of a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)
- a Self-managed user is someone who uses a Self-managed installation.
[FOSS LICENSE](https://gitlab.com/gitlab-org/gitlab/blob/master/LICENSE)

</details>

## Change Management

<details>
<summary markdown='span'>
  Adding new SKUs
</summary>

- When a new SKU is added, the Fulfillment team [must be notified thirty (30) days in advance](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/new?issuable_template=Sku) with the requirements so that they can prepare the customer applications.

</details>

<details>
<summary markdown='span'>
  SFDC Sandbox Refresh
</summary>

- When the SFDC sandbox is refreshed, the Fulfillment team is notified and the SFDC sandbox is reintegrated with the Zuora sandbox.

</details>

<details>
<summary markdown='span'>
  History of Change
</summary>

- [google doc](https://docs.google.com/document/d/1hWL7qJ2K0_1UH4CjNIyAWmcEVH6nlVf0svsDUxU89lk/edit)

</details>

## Customer Success and Billing

##### related resources: licensing, billing, transactions

- [CustomersDot and Customer web store: General Issues](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/8)
- [Issue: Analysis: Licenses & Terms](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/6)
- [EULAs/TOS: As-Is + To-Be](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/3)
- [Licenses/Self-Managed: As-Is + To-Be](https://gitlab.com/gitlab-com/business-ops/bizops-bsa/general-analysis/issues/4)
- [Troubleshooting: True Ups, Licenses + EULAs](/handbook/business-ops/business_systems/applications/troubleshooting/)
- [Troubleshooting subscription and licensing problems](/handbook/support/workflows/license_troubleshooting.html)
- [License documentation](https://docs.gitlab.com/ee/user/admin_area/license.html)
- [CustomersDot: Admin internal documentation](/handbook/internal-docs/customers-admin/)
- [Epic: Analysis: License and Billing Queue](https://gitlab.com/groups/gitlab-com/business-ops/bizops-bsa/-/epics/6)

##### refunds

<details>
<summary markdown='span'>
  When might [Billing cancel and/or downgrade a subscription](/handbook/finance/accounting/#zuora-subscription-status-active)?
</summary>

- request from the customer to have it cancelled as they don’t want to use it in the future
- customer bought the wrong number of seats and we refund due to this; we would not downgrade as we know that the customer will repurchase soon and we don’t want to cause disruption
- we need to change the entity of an account at renewal (sales assisted only).
    In this case, the subscription will be cancelled and a new one will be created under a new account.
- if credit card payment is declined for auto-renewed accounts (SaaS)

</details>

## Licensing and Renewals (L&R) Queue in Zendesk

The CustomersDot intersects with Support, Billing, Sales, and Product primarily.
Tickets come into the L&R queue in Zendesk generally three ways, generally prioritized as low:

- by emailing [renewals@gitlab.com](mailto:renewals@gitlab.com)

- by [opening a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487) and selecting `Help with my license`

- by filling out the form on the [Renewals page](https://about.gitlab.com/renewals/)
    The L&R Queue is a separate queue of Zendesk tickets that fall outside of the scope of customer product support; it's handled by Support by triaging the issue and rerouting the tickets to the appropriate team.

- **Passing to Sales:** Pass tickets relating to IACV to Sales via cc to Sales in reply to ticket which sends an email to both customer and account owner. If an account is associated with the ticket, there's a ticket object in SFDC.
    It's then Sales's responsibility to continue the customer interaction and the BSS closes the Zendesk ticket.

- **Passing to Sales:** If a ticket comes in and Support is unable to determine the account owner or the inquiry is related to new business, they reach out to the SFDC chatter group `ZD-newbusiness` where the SDR Regional leads for EMEA, APAC and AMER/LATAM provide an answer.
    Then Support cc's that person on the ticket and closes in Zendesk.
        - Regional Leads for SDRs:
            - AMER/LATAM - Mona Elliot
            - APAC - Jay Thomas-Burrows
            - EMEA - Elsje Smart

- **Passing to Billing:** The mechanism to pass tickets to the Billing team is to open the ticket, then change the field `form` from the value `Upgrades, Renewals & AR (refunds)` to `Accounts Receivable/Refunds`.
    This puts the ticket into the Billing Team's queue which they then manage.

## Defined Technology Approach

##### sales operations

Directly Responsible Individual James Harrison

- Clear, consistent workflows for Sales to be trained on new business and renewals.
- Clear, consistent processes on how to get self-service support or Sales support.

##### growth

Directly Responsible Individuals Amanda Rueda and Michael Karampalas

- Customers and Sales to be provided with easy to read format of known bugs that are affecting the CustomersDot, what is the workaround, how to ask for the workaround to be applied to them, and what the timeline to fix is.
- Improvements to the web store’s user interface, user experience, and backend processes and integrations.
- Provide regular and clear updates to the Sales and Customer Success organizations about work in progress and upcoming work that has an impact on those customer facing organizations.

##### support

Directly Responsible Individuals Donique Smit and Shaun McCann

- Support, triage and work the Zendesk License and Renewal queue.
- Provide feedback to stakeholders.
- Document known bugs and workarounds in appropriate locations as identified by Product.
- Participate in single lane channel identified by Sales to request help from Support.
- Work with Sales Systems team to resolve Salesforce synchronization problems.

##### enterprise applications

Directly Responsible Individual Jamie Carey

- Provide support to Sales, Support, and Product in creating interdepartmental feedback loops, workflows, and processes.
- Continue to monitor and illuminate gaps.
- Document the relationship between the CustomersDot and the product for Docs and Sales to provide to the customer during onboarding.
- Work with Product, Sales, and Support on a unified list of product features and functionality in prioritized order for Product.

##### enterprise applications integrations

Directly Responsible Individual Daniel Parker

##### finance operations

Directly Responsible Individual Alex Westbrook

- Primary administration of Zuora.
- Work with Sales Systems and Growth team to identify gaps, improve integrations between systems.
- Participate in end to end testing of enterprise application ecosystem.

##### sales systems

Directly Responsible Individual Jack Brennan

- Primary administration of Salesforce.
- Work with Finance Operations and Growth team to identify gaps, improve integrations between systems.
- Participate in end to end testing of enterprise application ecosystem.

## Growth Team

| [Growth Group](https://about.gitlab.com/direction/#growth-groups) | Request Type                                                                |
| ----------------------------------------------------------------- | --------------------------------------------------------------------------- |
| [Acquisition](https://about.gitlab.com/direction/acquisition/)    | new biz                                                                     |
| [Conversion](https://about.gitlab.com/direction/conversion/)      | trials                                                                      |
| [Expansion](https://about.gitlab.com/direction/expansion/)        | How do I?, upgrades                                                         |
| [Fulfillment](https://about.gitlab.com/direction/fulfillment/)    | purchasing issues/requests, CustomersDot usage, subscription management |
| [Retention](https://about.gitlab.com/direction/retention/)        | renewal, getting started, license, user management                          |

