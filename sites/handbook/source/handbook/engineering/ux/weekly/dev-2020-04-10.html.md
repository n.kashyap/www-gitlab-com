---
layout: handbook-page-toc
title: "Dev Section UX Weekly Update"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# 2020-04-10
## Team Updates

The Dev UX team has undergone some exciting changes!

- [Michael Le](https://gitlab.com/mle) has joined GitLab as Sr Product Designer, [Create:Static Site Editor](/handbook/product/product-categories/#static-site-editor-group)!
- Ecoystem moved to [Dev:Create](/handbook/product/product-categories/#create-stage)
  - This brought [Libor Vanc](https://about.gitlab.com/company/team/#lvanc) onto the Dev UX team!
- [Pajamas](https://design.gitlab.com/) will now be shepherded by the new [FE/UX Foundations](https://about.gitlab.com/direction/create/ecosystem/frontend-ux-foundations/) category!
  - [Taurie Davis](https://about.gitlab.com/company/team/#tauriedavis) is now (once again) a UX Manager!
  - [Jarek Ostrowski](https://about.gitlab.com/company/team/#jareko) joined Foundations!
  - [Jeremy Elder](https://about.gitlab.com/company/team/#jeldergl) also joined Foundations!
- [Marcel van Remmerden](https://about.gitlab.com/company/team/#mvanremmerden) is now (once again) Interim UX Manager for [Dev:Create](/handbook/product/product-categories/#create-stage)
  - Marcel will become UX Manager in June!
  - This made an opening on the [Create:Editor](/handbook/product/product-categories/#editor-group) group, which brings [Mike Nichols](https://about.gitlab.com/company/team/#mikenichols) to the Dev UX team!
- [Mike Long](https://about.gitlab.com/company/team/#mikelong) is now UX Manager of Dev:Plan and Dev:Manage!
- Whew! Did we miss anyone?

## Interestings

### North Star Metrics
We had our first North Star Metrics conversation! The North Star Metric is the one metric that matters: The most important metric a team focuses on.

- GitLab Unfiltered: [North Star Metrics, Dev - 2020-04-09](https://www.youtube.com/watch?v=Z4xdzSUFUns&list=PL05JrBw4t0Ko7HO427nXkoz9kovi_2dBi&index=2&t=0s)
- Slides: [2020-04-09 Dev Section North Star Metrics Review](https://docs.google.com/presentation/d/1NxUJZnW2psFFDGW94sjgowIw3QL60qlVpQHC9wkMsrk)

### Stale MRs Automation
In the recent Development group conversation, there was an interesting discussion about an analysis done on MRs that sit idle for too long. 

Observations/Recommendations that came from the analysis:

- Number of threads per MR
  - There is a correlation between # of threads and time to merge
  - Late MR's have more than two-fold more threads than non-late ones
  - If a merge request has >20 threads, a bot should add a comment recommending that the participants consider a synchronous session to discuss feedback, moving less critical feedback to issues to be handled later, etc.
- If an MR is idle for > 2 weeks, a bot should add a comment stating this with recommendations (consider splitting into smaller iterative changes, reassigning to even out workload, moving non-critical review feedback to separate issues, etc.)
- If an MR is idle for > 4 weeks, a bot should add a comment tagging responsible group manager with recommendations.

Some automation is coming that will identify idle MRs, and that initiative can be tracked here: [gitlab-org/quality/triage-ops/-/issues/453](https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/453).

### SUS Survey
We track our System Usability Scale (SUS) every quarter, and it's one of our [department metrics](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability).

A summary of the most recent SUS results, including open comments from respondents, is here: [System Usability Scale (SUS) Survey - Q4 2020](https://gitlab.com/gitlab-org/uxr_insights/-/issues/937).

The next SUS survey will go out soon. You can track the progress here: [System Usability Scale - Q1 FY21](https://gitlab.com/gitlab-org/ux-research/-/issues/783). Thank you, [Katherine Okpara](https://gitlab.com/katokpara)!

How do we improve our SUS metric? By continuing to action on [UX Scorecards](/handbook/engineering/ux/ux-scorecards/) for every [stage group](/handbook/product/product-categories/#devops-stages).

### New Label for UX Scorecard Recommendations
We noticed how difficult it is to track scorecard recommendations, and it was hard to know which ones are open or implemented. To that end, we've created a new label: [`UX scorecard-rec`](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UX%20scorecard-rec).

Making a recommendation is phase 2 in the UX Scorecard method. Here's the complete guide for how to run them: [/handbook/engineering/ux/ux-scorecards/](/handbook/engineering/ux/ux-scorecards/)

### Category Maturity (CM) Scorecards
We now have a scorecard method for validating category maturity: [/handbook/engineering/ux/category-maturity-scorecards/](/handbook/engineering/ux/category-maturity-scorecards/). 

We use this process to grade the [maturity of our products](https://about.gitlab.com/direction/maturity/) based on user performance and feedback.

### How to Contribute (for Product Designers)
Have you ever wanted to make a change to the GitLab UI and you weren't sure how? Check out this MR and give it a try! Reply in the MR if you run into any problems or want to give some feedback on the steps: [gitlab-com/www-gitlab-com/-/merge_requests/45797](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/45797) 

## Fun Stuff
### Update your Zoom client
Thanks to Zoom's growth from 10MM users late last year to over 200MM users in March, [Zoombombing](https://www.cnet.com/how-to/prevent-zoombombing-change-these-4-zoom-settings-now-for-secure-video-chat/) has become a huge problem on their video conferencing platform.

Check the handbook update for tips on privacy and security: [Zoom Privacy and Security](/handbook/tools-and-tips/zoom/#a-note-on-privacy-and-security)

The COVID-19 response has been great for their stock price though, [once people figured out which symbol to trade](https://www.nasdaq.com/articles/the-sec-really-wants-investors-to-stop-buying-the-wrong-zoom-stock-2020-03-27)!

