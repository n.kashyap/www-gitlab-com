---
layout: handbook-page-toc
title: Configure Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For an understanding of where this team is going, take a look at [the product](/direction/configure/)
vision.

As a member of the Ops Sub-department, you may also like to understand [our](/direction/ops/)
overall vision.

## Mission

The Configure group is responsible for developing Ops-focused features of GitLab
that relate to the "Configuration" and "Operations" stages of the DevOps
lifecycle. These refer to the configuration of infrastructure, and the running
of applications deployed via GitLab.

This team is currently building out more features for our Kubernetes integration,
including the [Auto DevOps](/product/auto-devops/) feature set, and making it easier for
GitLab users to make the most of Kubernetes and DevOps best practices.

As per the [product categories](/handbook/product/categories/), this team
is responsible for building out new feature sets that will allow
GitLab users to easily make use of the following modern DevOps practices:

- Auto DevOps
- Kubernetes Configuration
- Infrastructure as Code
- Serverless

We work collaboratively and transparently, and we will contribute as much of our
work as possible back to the open source community.

### Product Indicators

Value is measured by **the number of deployment pipelines going through configure categories's features (**either K8s, Auto DevOps, Serverless or IaC in general). This is a good approximation of customer value because GitLab users ultimately want either their infrastructure or something of their infrastructure deployed.

We break down this metric into input metrics by product category and might come up with a further breakdown of the metrics when necessary.

#### AARRR for Kubernetes Management

``` mermaid
classDiagram
  Acquistion --|> Activation
	Acquistion : Are users aware of the product or feature set?
	Acquistion: Measurement (Count of Operations/Kubernetes docs views)
  Activation --|> Retention
	Activation : Are users applying the feature?
	Activation: Measurement (Count of the number of the registered Kubernetes Agents with valid tokens)
  Retention --|> Revenue
	Retention : Are users applying the feature over time?
	Retention: Measurement (Count of the number of GitOps synchronization operations)
  Revenue --|> Referral
	Revenue : Are users paying for the features?
	Revenue: Measurement (# of paying users that have installed an agent / Total users that have installed an agent.)
  Referral --|> Acquistion
	Referral : Are users encouraging others to use the feature?
	Referral: Measurement (Count of pageviews on Kubernetes cluster documentation coming from referrals)
```

#### AARRR for Infrastructure as Code

We have two features that we follow to understand the health of our funnel:
- Terraform reports can be used by any Terraform user on GitLab, and is a lightweight integration of GitLab and Terraform
- GitLab Managed Terraform State provides the deepest Terraform integration experience available

The following graph shows the metrics defined for Terraform reports. We want to track the same reports for the GitLab Managed Terraform State.

``` mermaid
classDiagram
  Acquistion --|> Activation
	Acquistion : Are users aware of the product or feature set?
	Acquistion: Measurement (Count of pageview on Infrastructure as Code documentation)
  Activation --|> Retention
	Activation : Are users applying the feature?
	Activation: Measurement (Number of projects with Terraform reports in the last month)
  Retention --|> Revenue
	Retention : Are users applying the feature over time?
	Retention: Measurement (Number of Terraform reports generated in the last month)
  Revenue --|> Referral
	Revenue : Are users paying for the features?
	Revenue: Measurement ((# of paying users that have Terraform reports / Total users that have have Terraform reports.))
  Referral --|> Acquistion
	Referral : Are users encouraging others to use the feature?
	Referral: Measurement (Count of pageviews on Infrastructure as Code documentation coming from referrals)
```

## Team Members

<%= direct_team(manager_role: 'Backend Engineering Manager, Configure') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Configure/, direct_manager_role: 'Backend Engineering Manager, Configure') %>

## Common Links

- [Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1223426?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Aorchestration)
- [Slack Team Channel](https://gitlab.slack.com/archives/g_configure_orchestration)
- [Slack Standup Channel](https://gitlab.slack.com/archives/s_configure_standup)
- [How we structure our day](https://gitlab.com/gl-retrospectives/configure/issues/17)

## Processes

### Planning

We use planning issues to work out the planning for the next milestone asynchronously.

1. The PM opens up a new milestone planning issue at the start of every milestone, and lists the issues that we should be working on in priority order.
1. All stakeholders (PM, UX, Engineering) are invited to collaborate on the planning for the next milestone.
1. The Engineering Managers review the issues to determine whether there will be sufficient capacity to deal with all the issues listed.
1. EMs also add bugs to the milestone that should be worked, based on Priority and Severity labels.

Example planning issue: https://gitlab.com/gitlab-org/configure/general/issues/6

### Scheduling

#### Feature development

Our goal is to move towards a continuous delivery model so the team completes tasks regularly, and keeps working off of a prioritized backlog of issues. We default to team members self-scheduling their work:

- The team has weekly meetings to discuss issue priorities and to do issue refinement.
- Team members self-assign issues from the [Configure issue board](https://gitlab.com/groups/gitlab-org/-/boards/1223426?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Aorchestration) that are in the `workflow:ready for development` column.
- Once a team member has completed their assigned issues, they are expected to go to the group issue boards and assign themselves to the next unassigned `workflow:ready for development` issue.
- The issues on the board are in priority order based on importance (the higher they are on the list, the higher the priority). This order is set by the product manager.
- If all issues are assigned for the milestone, team members are expected to identify the next available issue to work on based on the team's work prioritization (see below).
- While backstage work is important, in the absence of specific prioritization, the team will have a bias towards working on `bug` or `feature` categorized issues.

#### Bug fixing and priortized work

In addition to the self-scheduling of feature development, the manager will from time to time assign bugs, or other work deemed important, directly to a team member.

#### Iteration

We rely heavily on iteration as part of our workflow and aim to deliver the smallest possible MVC in the shortest period of time.

We use the following practices to enhance iteration on our team:

- Promoting large issues to epics and isolating specific pieces into separate issues
- Delivering issues behind a feature flag to maximize throughput
- Develop quick POCs to vet functionality and brainstorm product/market fit
- README driven development

### MR reviews

Team members should use their best judgment to determine whether to assign the first review of an MR based on the DangerBot's suggestion or to someone else on the team. Some factors in making this decision may be:

- If there is known domain expert for the area of code, prefer assigning the initial review to them.
- Does the MR require a lot of context to understand what it is doing? Will it take a reviewer outside the team a long time to ramp up on that context?
- Does the MR require a lot of dev environment setup that team members outside the Configure stage are likely to not have?
- Is the MR part of a larger effort for which a specific team member already has all the context?

### Backend & Frontend Issue Collaboration

Our team follows the GitLab [workflow guidelines for working in teams](/handbook/engineering/workflow/#working-in-teams).

Given a reasonably sized issue requiring UX, frontend and backend work, the preferred way to collaborate on the issue is as follow:

1. Once an issue is labeled `workflow::ready for development`, backend is usually able to start working on the issue.
    1. This is a good time to discuss and clarify interdependency between backend and frontend.
1. Backend and frontend will work on the issue in separated branches.
    1. Each will submit their own MR for review.
    1. Frontend and backend will put the feature behind a feature flag.
1. Once both MRs have been merged, a third MR will be opened to integrate the work where backend and frontend will collaborate to:
    1. Remove the feature flag.
    1. Add documentation.
    1. Implement e2e tests for the feature.

The above is a guideline and clear communication should be preferred over process to ensure the best collaboration strategy on an issue. For example on smaller issues, or where the frontend component of the work is minor, it may be feasible to work on the same branch.

## How to work with us

### How to contribute to Auto DevOps

Read our [specific GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md)
instructions
on how to develop features for Auto DevOps.

### Useful links for contributing to Auto DevOps

- [Tips and Troubleshooting](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops/tips_and_troubleshooting.md)
- [Useful Commands](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops/useful_commands.md)
- [How to work with slow connections](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops/tips_and_troubleshooting.md#qa)
- [Enabling premium features for development purposes](https://license.gitlab.com/users/sign_in)