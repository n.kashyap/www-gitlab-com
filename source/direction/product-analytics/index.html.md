---
layout: markdown_page
title: Product Direction - Product Analytics
description: "Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-analytics/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. These technologies include but are not limited to: our in-house tool called [Usage Ping](https://docs.gitlab.com/ee/development/product-analytics/usage_ping.html), [Snowplow Analytics](https://docs.gitlab.com/ee/development/product-analytics/snowplow.html), and [`version.gitlab.com`](https://gitlab.com/gitlab-services/version-gitlab-com) which acts as our collector for Usage Ping and enables [ Version Check](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#version-check).

The primary purpose of Product Analytics is to help us build a better Gitlab. Data about how Gitlab is used is collected to better understand what parts of Gitlab needs improvement and what features to build next. Product Analytics also helps our team better understand the reasons why people use Gitlab and with this knowledge we are able to make better product decisions.

The overall vision for the Product Analytics group is to ensure that we have a robust, consistent and modern data collection framework in place to best serve our internal Product, Finance, Sales, and Customer Success teams. The group also ensures that GitLab has the best visualization and analysis tools in place that allows the best possible insights into the data provided through the various collection tools we utilize.

If you'd like to discuss this vision directly with the Product Manager for Product Analytics, feel free to reach out to [@jeromezng](https://gitlab.com/jeromezng).

| Category     | Description                                                                                        |
|--------------|----------------------------------------------------------------------------------------------------|
| Collection   | The tooling and framework for how we collect product usage data.                                   |
| Analysis     | The tooling and framework for how we analyze product usage data.                                   |

### Collect Data 

```mermaid
graph LR
    privacy_policy[Privacy Policy] --> privacy_settings[Privacy Settings]
    privacy_settings[Privacy Settings] --> collection_framework[Collect Framework]
    collection_framework[Collect Framework] --> event_dictionary[Event Dictionary]
    event_dictionary[Event Dictionary] --> instrumentation[Instrumentation]
    instrumentation[Instrumentation] --> data_availability[Data Availability]
    data_availability[Data Availability] --> data_warehouse[Data Warehouse]
    data_warehouse[Data Warehouse] --> data_modelling[Data Modelling]
```

### Enable Product
```mermaid
graph LR
    sisense_dashboard[Sisense Dashboard] --> performance_indicators[Performance Indicator]
    performance_indicators[Performance Indicator] --> pi_target[PI Target]
    pi_target[PI Target] --> pi_review[PI Review]
    pi_review[PI Review] --> improve_product[Improve Product]
```

### Enable Sales and Customer Success

```mermaid
graph LR
    salesforce_data_feed[Salesforce Data Feed] --> salesforce_dashboard[Salesforce Dashboard]
    salesforce_dashboard[Salesforce Dashboard] --> gainsight_dashboard[Gainsight Dashboard]
    gainsight_dashboard[Gainsight Dashboard] --> customer_conversations[Customer Conversations]
```

### Make Data Useful for Customers

```mermaid
graph LR
    value[Salesforce Data Feed] --> salesforce_dashboard[Salesforce Dashboard]
    salesforce_dashboard[Salesforce Dashboard] --> gainsight_dashboard[Gainsight Dashboard]
    gainsight_dashboard[Gainsight Dashboard] --> customer_conversations[Customer Conversations]
```

## Roadmap FY21-Q3

| Theme                                                                                                 | Teams Involved                                    |
|-------------------------------------------------------------------------------------------------------|---------------------------------------------------|
| [Lead the Product Performance Indicators OKR](#product-performance-indicators)                        | Product Analytics PM, Data, Product                       |
| [Deploy Product Analytics for Sales and Customer Success](#deploy-product-analytics-for-sales-and-customer-success)   | Product Analytics PM, Data, Sales, Customer Success       |
| [Research Privacy Policy for Product Usage Data](#rollout-privacy-policy-for-product-usage-data)      | Product Analytics PM, Data, Legal, Security               |
| [Build Collection Framework](#collection-framework)                                                   | Product Analytics Engineering                             |
| [Scale and Maintain Usage Ping](#scale-and-maintain-usage-ping)                                       | Product Analytics Engineering                             |
| [Instrument Tracking](#instrument-tracking)                                                           | Product Analytics PM / Engineering, Data, Product, Sales  |

### Lead the Product Performance Indicators OKR

Lead the [Product Org OKR's for Q3](https://gitlab.com/gitlab-com/Product/-/issues/1320). For more information, see [Product Performance Indicators](https://docs.google.com/presentation/d/1wCpvdCUtBtU4Y1vHDOSOLjhrlPvdYBAbydj58SRN5Js/edit).

- KR1 (EVP, Product): [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- KR2 (EVP, Product): [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343)

**Deliverables:**

- [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)
- [Add All PIs to Product PI Pages](https://gitlab.com/groups/gitlab-com/-/epics/906)

### Deploy Product Analytics for Sales and Customer Success

Support the [FY21-Q3 Deploying Product Analytics for CRO Org](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#). For more information, see [CRO Product Analytics: Status, Gaps and the Road Forward](https://docs.google.com/document/d/17dw3qpX5PbvF_WwQXNEQuCPqGUcng1zy85R-2fIL1k8/edit#).

**Deliverables:**

- [Customer Adoption Journey](https://gitlab.com/groups/gitlab-org/-/epics/3572)

### Research Privacy Policy for Product Usage Data

Support the [Privacy Policy Rollout](https://gitlab.com/groups/gitlab-com/-/epics/907)

**Deliverables:**

- [Privacy Policy for Product Usage Data](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8780)

### Build Collection Framework

**Current State:**

1. Usage Ping (SaaS + SM). **Not useful for multi-tenant SaaS as it’s missing Group (namespace) and Plan.**
1. Snowplow (SaaS). **Not identifiable to a Group or User and cannot be used for UMAU. Eventually available on self-managed.**
1. Database Imports (SaaS). **Not available on self-managed.**

![](collection_framework_fy21_q3.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

**Roadmap:**

1. Q3: Usage Ping Redis (SaaS + SM).
1. Q3: Plan-level reporting of Usage Ping (SaaS).
1. Q4: User-level reporting on Snowplow (SaaS + SM).
1. Q4: Group-level reporting on Snowplow (SaaS + SM).
1. Q4: Snowplow (SM).
1. FY22-Q1: Group-level reporting on Usage Ping (SaaS).

![](collection_framework_fy21_q4.png)

✅ Available, 🔄 In Progress, 📅 Planned, ✖️ Not Planned

[Source file](https://docs.google.com/spreadsheets/d/1e8Afo41Ar8x3JxAXJF3nL83UxVZ3hPIyXdt243VnNuE/edit#gid=0)

### Scale and Maintain Usage Ping

**Current State:**

- Generating a Usage Ping on GitLab.com now takes **over 24 hours**. This is a **120% increase** from the [11 hours](https://gitlab.com/gitlab-org/gitlab/-/issues/228571#note_388866586) it previously took when we initially fixed and re-enabled Usage Ping five months ago. This increase in generation time is due to the amount of new Usage Ping counters we’ve added along with the rapidly growing size of GitLab.com’s database tables. The payload size of Usage Ping has [increased from 376 metrics to 544+ metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/228571#note_388930798).
- Usage Ping currently does not have a retry or caching mechanism. After spending the time generating a Usage Ping, if a network error occurs when passing sending data to us, the Usage Ping is lost and we will need to wait for the next scheduled cron job 7 days later.

**Roadmap:**

- Compute Usage Ping counters in parallel instead of serially.
- Add retry logic and caching for Usage Pings.

### Instrument Tracking

**Current State:**

- To support the [Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/) work, we need to guide and review each product group’s tracking code. In some cases, we will need to directly do the implementation work.
- Currently Usage Ping is not segmented by plan type which means for any SaaS free / paid segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense.
- Currently, for our SMAU / Section MAU metrics, we use the highest component in each grouping. For example, for SMAU, we take the highest GMAU in the group. This methodology of calculating SMAU / Section MAU is lacking as uses one metric to represent the whole group instead of finding the union across the entire group.

**Roadmap:**

- Support [Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/). This will help us achieve Product’s Q3 OKR.
- [Enable Plan-level reporting of SaaS Usage Ping](https://gitlab.com/gitlab-org/product-analytics/-/issues/423). This will allow Usage Ping to be separated by free and paid on SaaS so that the data team doesn’t need to manually recreate all Usage Ping queries in Sisense.
- [De-duplicate GMAU  / SMAU /metrics](https://gitlab.com/gitlab-org/product-analytics/-/issues/421) on the self-managed instance before the Usage Ping is sent. This will help us achieve more accurate XMAU metrics.

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Analytics Product Direction](/direction/product-analytics/)                                                                              | The roadmap for Product Analytics at GitLab                       |
| [Acquisition, Conversion, Product Analytics Development Process](/handbook/engineering/development/growth/conversion-product-analytics/) | The development process for the Acquisition, Conversion, and Product Analytics groups         |
| [Product Analytics Guide](https://docs.gitlab.com/ee/development/product-analytics/index.html)                                                    | An overview of our collection framework                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](https://docs.gitlab.com/ee/development/product_analytics/event_dictionary.html)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Product Performance Indicators Workflow](/handbook/product/performance-indicators#product-analytics-workflow)                                   | The workflow for putting product PIs and XMAUs in place   |
| [Data Team: Creating Charts](/handbook/business-ops/data-team/programs/data-for-product-managers/)                                | How to create your own dashboard                          |
| [Data Team: Data Warehouse](/handbook/business-ops/data-team/platform/#data-warehouse)                                            | An outline of where our product usage data is stored      |
| [Data Team: dbt Guide](/handbook/business-ops/data-team/platform/dbt-guide/)                                                      | How we transform raw data into a data structure that's ready for analysis |
| [Growth Product Direction](/direction/growth/)                                                                                    | The roadmap for Growth at GitLab                          |
| [Growth Product Process](/handbook/product/growth/)                                                                              | The product process for the Growth sub-department         |
| [Growth Sub-Department Development Process](/handbook/engineering/development/growth/).                                              | The development process for the Growth sub-department     |
| [Growth Sub-Department Performance Indicators Page](/handbook/engineering/development/growth/performance-indicators/)              | The performance indicators for the Growth sub-department  |
| [Growth UX Process](/handbook/engineering/ux/stage-group-ux-strategy/growth/)                                                        | The UX process for the Growth sub-department              |
| [Growth QE Process](/handbook/engineering/quality/growth-qe-team/)                                                                   | The QE process for the Growth sub-department              |

